clear;
clc;
%%% Querysize 3 %%%
fileID = fopen('NAlgoData_Q3_n200_s01.txt', 'r');
sizeH = [5 inf];
H1 = fscanf(fileID, '%f', sizeH);
fclose(fileID);
H1 = H1';

fileID = fopen('NAlgoData_Q3_n200_s03.txt', 'r');
sizeH = [5 inf];
H2 = fscanf(fileID, '%f', sizeH);
fclose(fileID);
H2 = H2';

fileID = fopen('NAlgoData_Q3_n200_s05.txt', 'r');
sizeH = [5 inf];
H3 = fscanf(fileID, '%f', sizeH);
fclose(fileID);
H3 = H3';

fileID = fopen('NAlgoData_Q3_n200_s07.txt', 'r');
sizeH = [5 inf];
H4 = fscanf(fileID, '%f', sizeH);
fclose(fileID);
H4 = H4';

fileID = fopen('NAlgoData_Q3_n200_s09.txt', 'r');
sizeH = [5 inf];
H5 = fscanf(fileID, '%f', sizeH);
fclose(fileID);
H5 = H5';

%%% Querysize 5 %%%
fileID = fopen('NAlgoData_Q5_n200_s01.txt', 'r');
sizeH = [5 inf];
H1a = fscanf(fileID, '%f', sizeH);
fclose(fileID);
H1a = H1a';

fileID = fopen('NAlgoData_Q5_n200_s03.txt', 'r');
sizeH = [5 inf];
H2a = fscanf(fileID, '%f', sizeH);
fclose(fileID);
H2a = H2a';

fileID = fopen('NAlgoData_Q5_n200_s05.txt', 'r');
sizeH = [5 inf];
H3a = fscanf(fileID, '%f', sizeH);
fclose(fileID);
H3a = H3a';

fileID = fopen('NAlgoData_Q5_n200_s07.txt', 'r');
sizeH = [5 inf];
H4a = fscanf(fileID, '%f', sizeH);
fclose(fileID);
H4a = H4a';

fileID = fopen('NAlgoData_Q5_n200_s09.txt', 'r');
sizeH = [5 inf];
H5a = fscanf(fileID, '%f', sizeH);
fclose(fileID);
H5a = H5a';

%%% Querysize 7 %%%

fileID = fopen('NAlgoData_Q7_n50_s01.txt', 'r');
sizeH = [5 inf];
H1b = fscanf(fileID, '%f', sizeH);
fclose(fileID);
H1b = H1b';

fileID = fopen('NAlgoData_Q7_n50_s03.txt', 'r');
sizeH = [5 inf];
H2b = fscanf(fileID, '%f', sizeH);
fclose(fileID);
H2b = H2b';

fileID = fopen('NAlgoData_Q7_n50_s05.txt', 'r');
sizeH = [5 inf];
H3b = fscanf(fileID, '%f', sizeH);
fclose(fileID);
H3b = H3b';

fileID = fopen('NAlgoData_Q7_n50_s07.txt', 'r');
sizeH = [5 inf];
H4b = fscanf(fileID, '%f', sizeH);
fclose(fileID);
H4b = H4b';

fileID = fopen('NAlgoData_Q7_n50_s09.txt', 'r');
sizeH = [5 inf];
H5b = fscanf(fileID, '%f', sizeH);
fclose(fileID);
H5b = H5b';

%%% Influence of the querysize (with fixed tree depth = 3). 
H_Q3 = [H1; H2; H3; H5];
H_Q5 = [H1a; H2a; H3a; H5a];
H_Q7 = [H1b; H2b; H3b; H5b];
figure;
subplot(1,3,1)
boxplot(H_Q3, 'OutlierSize',2,'Labels', {'Avg.', 's-IC', 's-IR', 's-E', 's-EIR'});
ylim([0.5 1.02])
set(gca,'YTick',0.5:.1:1)
xlabel('Querysize = 3')
ylabel('Algo(SI)/OPT(SI)')

subplot(1,3,2)
boxplot(H_Q5, 'OutlierSize',2,'Labels', {'Avg.', 's-IC', 's-IR', 's-E', 's-EIR'})
ylim([0.5 1.02])
set(gca,'YTick',0.5:.1:1)
xlabel('Querysize = 5')

subplot(1,3,3)
boxplot(H_Q7, 'OutlierSize',2,'Labels', {'Avg.', 's-IC', 's-IR', 's-E', 's-EIR'})
ylim([0.5 1.02])
set(gca,'YTick',0.5:.1:1)
xlabel('Querysize = 7')

%%% Influence of the samplerate (with fixed tree depth).

Hs1 = [H1; H1a; H1b];
Hs3 = [H2; H2a; H2b];
Hs5 = [H3; H3a; H3b];
Hs7 = [H4; H4a; H4b];
Hs9 = [H5; H5a; H5b];

ms1 = mean(Hs1);
ms2 = mean(Hs3);
ms3 = mean(Hs5);
ms4= mean(Hs7);
ms5 = mean(Hs9);

M = [ms1; ms2; ms3; ms4; ms5];
samples = [0.1, 0.3, 0.5, 0.7, 0.9];
figure
plot(samples, M(:,1),'-.r', 'LineWidth', 1.5)
hold on
plot(samples, M(:,2),'-bd','LineWidth', 1.5)
hold on
plot(samples, M(:,3),'-gd','LineWidth', 1.5)
hold on
plot(samples, M(:,4),'-cd','LineWidth', 1.5)
hold on
plot(samples, M(:,5),'-kd','LineWidth', 1.5)
xlabel('sample ratio s')
ylabel('average Algo(IR)/OPT(IR)')
set(gca, 'XTick', samples)  

%%% Timing experiment. 
fileID = fopen('Nobaseline_TimingQuery3.txt', 'r');
sizeH = [4 inf];
T1 = fscanf(fileID, '%f', sizeH);
fclose(fileID);
T1 = T1';

fileID = fopen('Nobaseline_TimingQuery5.txt', 'r');
sizeH = [4 inf];
T2 = fscanf(fileID, '%f', sizeH);
fclose(fileID);
T2 = T2';

fileID = fopen('Nobaseline_TimingQuery7.txt', 'r');
sizeH = [4 inf];
T3 = fscanf(fileID, '%f', sizeH);
fclose(fileID);
T3 = T3';

figureHandle = gcf;
bar([mean(T1); mean(T2); mean(T3)], 'BarWidth', 1);
legend('s-IC', 's-IR', 's-E', 's-EIR', 'Location', 'NorthWest');
set(gca,'XTickLabel',{'Querysize = 3','Querysize = 5','Querysize = 7'})
ylabel('Average run time (s)')


%%%% Jaccard Distance %%%%%
sizeJ = [4 inf];
fileID = fopen('CommonAuthorsQ5.txt', 'r');
Common5 = fscanf(fileID, '%f', sizeJ);
fclose(fileID);
Common5 = Common5';

fileID = fopen('CommonAuthorsQ7.txt', 'r');
Common7 = fscanf(fileID, '%f', sizeJ);
fclose(fileID);
Common7 = Common7';

fileID = fopen('CommonAuthorsQ9.txt', 'r');
Common9 = fscanf(fileID, '%f', sizeJ);
fclose(fileID);
Common9 = Common9';

