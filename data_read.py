import networkx as nx
import numpy as np
import matplotlib.pyplot as plt
import pickle

# Reading the meta data.
d_meta={}
d_index={} 
with open('citation-acm-v8.txt', 'r', encoding = 'utf8') as meta:
    teller = 0
    authors = []
    for line in meta:
        #print(line, end='')
        if '#*' in line:
            title = line[2:-1]
        elif '#@' in line:
            authors = line[2:-1]
        elif '#t' in line:
            year = line[2:-1] 
        elif '#index' in line:
            index_value = line[6:-1]
            d_index[index_value]=teller
            ref = []
        elif '#%' in line:
            ref.append(line[2:-1])
        elif line == '\n':
            d_meta[teller] = [title, authors, year, ref]
            teller += 1

        
# Creating the (unweighted) graph.
G = nx.DiGraph()
for i in d_meta:
    for j in d_meta[i][3]:
            try: G.add_edge(i, d_index[j])
            except: pass

# Reading & adding the weights of the graph G.
# These contain only the weights (EDGE PROBABILITIES!) of the actual edges present in the graph.


d_weights={}
with open('acm-weights.txt', 'r') as weights:
	for line in weights:
		vals = line.split("\t")
		d_weights[(int(vals[0]), int(vals[1]))]=float(vals[2])

d_weights_neutral = {} 		
with open('acm-weights.txt', 'r') as weights:
	for line in weights:
		vals = line.split("\t")
		d_weights_neutral[(int(vals[0]), int(vals[1]))]=0.02

d_weights_bin5 = {}
with open('acm-weightsDAG_bin5.txt', 'r') as weights:
	for line in weights:
		vals = line.split()
		d_weights_bin5[(int(vals[0]), int(vals[1]))]=float(vals[2])


d_weights_bin3 = {}
with open('acm-weightsDAG_bin3.txt', 'r') as weights:
	for line in weights:
		vals = line.split()
		d_weights_bin3[(int(vals[0]), int(vals[1]))]=float(vals[2])



# Opening the (unweighted) reverse graph 
with open('ReverseG_acm.pickle', 'rb') as handle:
    Grev = pickle.load(handle)

