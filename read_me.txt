# data_read.py is a script to load the ACM v8 citation network data, and the edge weights dicts. (according to different background distributions).
# graph_functions.py contains different kinds of functions.
 - SteinerDir1, SteinerDir2, SteinerDir3 and SteinerDir4 are the different heuristics.
 - createShortestPaths and Common are the two preprocessing steps.
 - GenerateData was used to create the experiments from Fig. 5&6 (see paper)
 - S2AllRoots was used to create Figure 1, 7 and 8.
 - computeAuthors was used to create Table 1.

# Fitting the model
 - acm-year, acm-connectivity and acm-nodes are all data files related to the acm data.
 - acm_data_DAGprior is used to fit the model (with DAG prior beliefs). 
# Experiments data
- NAlgoData_... are files related to the experiment from Fig. 5&6.
- Nobaseline_TimingQuery... are the timing files for this experiment.
- CommonAuthors... are text files related to the experiments in Table 1. 
- plotdata.m is used to generate figures.
