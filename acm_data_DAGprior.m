clear;
clc;

% Throwing out the non-connected nodes and relabeling them through a dict.
fileID = fopen('acm-nodes.txt', 'r');
sizeH = [2 Inf];
H = fscanf(fileID, '%i', sizeH);
fclose(fileID);
keys = H(1,:);
vals = H(2,:);
dict = containers.Map(keys, vals);
dictinv = containers.Map(vals, keys);
clear H;
clear keys;
clear vals;

% Reading the year.txt file.
fileID = fopen('acm-year.txt', 'r');
sizeY = [2 Inf];
Y = fscanf(fileID, '%i', sizeY);
fclose(fileID);
nodes = cell2mat(values(dict, num2cell(Y(1,:)))); % Relabeling the nodes.
years = Y(2, :);
clear Y;

% Binning the years data.
binranges = min(years):max(years);
[bincounts, ind] = histc(years,binranges); % ind assigns to each element in years the corresponding bin. ind = 0 implies unbinned element.
%figure
%bar(binranges,bincounts,'histc')



% Reading the connectivity files.
fileID = fopen('acm-connectivity.txt', 'r');
Aorig = fscanf(fileID, '%i', sizeH);
fclose(fileID);

Aconn = cell2mat(values(dict, num2cell(Aorig)));
A = sparse(Aconn(1,:), Aconn(2,:), ones(1, length(Aconn)), 1369055, 1369055);

[lar, lac, lalpha, jr, jc] = Newton_DAG(A, 40, ind, nodes);

%{
%%%% Writing the weights to a .txt file %%%
drows = full(sum(A,2));
dcols = full(sum(A,1))';

bins = cell(1, max(ind)); % Creating empty cells. 
nu_r = cell(1, max(ind));
nu_c = cell(1, max(ind));
j_r = cell(1, max(ind));
j_c = cell(1, max(ind));

l_r = zeros(1, max(ind));
l_c = zeros(1, max(ind));
occur_r = cell(1, max(ind));
occur_c = cell(1, max(ind));

for i=1:max(ind)
    bins{i} = nodes(ind==i); % Every cell element is a vector with the resp. bin nodes.
    [nu_r{i}, ~, j_r{i}] = unique(drows(bins{i})); % Finding the unique row degrees in the ith bin.
    [nu_c{i}, ~, j_c{i}] = unique(dcols(bins{i})); % Finding the unique column degrees in the ith bin.
    
    l_r(i) = length(nu_r{i}); % Finding the number of occurences of elements in nu_r in the ith bin.
    occur_r{i} = zeros(l_r(i),1);
    for j=1:l_r(i)
        occur_r{i}(j)=length(find(j_r{i}==j));
    end
    
    l_c(i) = length(nu_c{i}); % Finding the number of occurences of elements in nu_c in the ith bin.
    occur_c{i} = zeros(l_c(i),1);
    for j=1:l_c(i)
        occur_c{i}(j)=length(find(j_c{i}==j));
    end   
end

numberofbins = length(occur_r);
csumlr = cumsum(l_r);
csumlc = cumsum(l_c);

% Aconn is the dict version of the acm-connect. file
[~, bins_of_nodes] = ismember(1:1369055, nodes);
bins_of_nodes = ind(bins_of_nodes);
Aconn = Aconn';
AlphaParameter_edge = lalpha(numberofbins+bins_of_nodes(Aconn(:,1))-bins_of_nodes(Aconn(:,2)));

LAR = zeros(1369055,1);
LAC = zeros(1369055,1);

temp1 = lar(1:csumlr(1));
temp2 = lac(1:csumlc(1));
LAR(bins{1}) = temp1(j_r{1});
LAC(bins{1}) = temp2(j_c{1});
for i=2:length(bins);
    temp1 = lar(csumlr(i-1)+1:csumlr(i));
    temp2 = lac(csumlc(i-1)+1:csumlc(i));
    LAR(bins{i}) = temp1(j_r{i});
    LAC(bins{i}) = temp2(j_c{i});
end

RowParameter_edge = LAR(Aconn(:,1));
ColumnParameter_edge = LAC(Aconn(:,2));

WEIGHTS = exp(RowParameter_edge+ColumnParameter_edge+AlphaParameter_edge');
WEIGHTS = WEIGHTS./(1+WEIGHTS);
fileID = fopen('acm_BINPEARYEAR.txt', 'w');
fprintf(fileID,'%i %i %2.5e\r\n', [Aorig(1,:); Aorig(2,:); WEIGHTS']);
fclose(fileID);

%}