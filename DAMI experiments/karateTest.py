# Running the karate tests.
from matplotlib import pyplot as plt
from Functions import *
from numpy import array

loc = './13-sdm-dot2dot-paper-data/karate/karate.txt'
G = nx.read_weighted_edgelist(loc)

pos = {'20': array([ 0.59386092,  0.77274124]), '4': array([ 0.97030047,  0.38992532]), '7': array([ 0.57231154,  0.9947786 ]), '27': array([ 0.        ,  0.41694984]), '32': array([ 0.24166387,  0.54726601]),
       '16': array([ 0.13231291,  0.15142916]), '8': array([ 0.97444281,  0.58859054]), '23': array([ 0.31343762,  0.02912065]), '5': array([ 0.77284886,  0.91872272]), '25': array([ 0.06626896,  0.75746468]),
       '21': array([ 0.65946361,  0.0231295 ]), '1': array([ 0.72917635,  0.66496154]), '24': array([ 0.0689278 ,  0.35544954]), '3': array([ 0.7263457 ,  0.47635167]), '28': array([ 0.12370235,  0.52865312]),
       '6': array([ 0.48280968,  1.        ]), '18': array([ 0.9469069 ,  0.72231067]), '19': array([ 0.21460042,  0.08029374]), '34': array([ 0.42,  0.4 ]), '2': array([ 0.84078198,  0.49490659]),
       '12': array([ 0.87460967,  0.83053415]), '13': array([ 0.92535394,  0.23433861]), '31': array([ 0.62322762,  0.1517452 ]), '9': array([ 0.54685358,  0.31792909]), '29': array([ 0.25654835,  0.73812879]),
       '11': array([ 0.47380106,  0.8692976 ]), '26': array([ 0.00544214,  0.59611235]), '14': array([ 0.76674322,  0.24941683]), '33': array([ 0.32838666,  0.21078124]), '10': array([ 0.55566911,  0.        ]),
       '30': array([ 0.06098275,  0.24720181]), '15': array([ 0.42089088,  0.00060218]), '17': array([ 0.55408307,  1.1432101]), '22': array([ 0.99964549,  0.4954797 ])}

# pos = nx.spring_layout(G, k=0.25, pos=pos)

c1 = ['1','2','3','4','5','6','7','8','11','12','13', '14', '17', '18', '20', '22']
c2 = ['9','10','15','16','19','21','23','24','25','26','27','28','29','30','31','32','33','34']

## Plotting The Karate Network
plt.ioff()
xmin = -0.1
xmax = 1.1
ymin = -0.1
ymax = 1.2

plt.figure(1)
plt.axis('off')
axes = plt.gca()
axes.set_xlim([xmin,xmax])
axes.set_ylim([ymin,ymax])
nx.draw_networkx_nodes(G, pos, nodelist = c1, node_color = 'r')
nx.draw_networkx_nodes(G, pos, nodelist = c2, node_color = 'g')
nx.draw_networkx_edges(G, pos)
#nx.draw_networkx_labels(G, pos)
plt.savefig("C:\\Users\\fawadria\\Desktop\\Journal DAMI git\\Karate2.eps", bbox_inches='tight',pad_inches = 0, close=True)

d_weights={}
with open(loc, 'r') as weights:
    for line in weights:
        vals = line.split()
        d_weights[(int(vals[0]), int(vals[1]))]=0.2
        d_weights[(int(vals[1]), int(vals[0]))]=0.2

query = set(c2)

## Plotting Forest with neutral background weights
_, f1 = SteinerTree(G, query, 6, d_weights, len(G.nodes()))
edgelist = [(k, v) for k, v in f1.items() if v != None]
edgelistRemaining = [(k, v) for k, v in G.edges() if (k, v) not in edgelist and (v,k) not in edgelist]

plt.figure(2)
plt.axis('off')
axes = plt.gca()
axes.set_xlim([xmin,xmax])
axes.set_ylim([ymin,ymax])
nx.draw_networkx_nodes(G, pos, nodelist = c1, node_color = 'r')
nx.draw_networkx_nodes(G, pos, nodelist = c2, node_color = 'g')
nx.draw_networkx_edges(G, pos, edgelist, width=2)
nx.draw_networkx_edges(G, pos, edgelistRemaining, alpha = 0.2)
#nx.draw_networkx_labels(G, pos)
plt.savefig("C:\\Users\\fawadria\\Desktop\\Journal DAMI git\\KarateNoPrior.pdf", bbox_inches='tight',pad_inches = 0, close=True)

## Plotting Forest with Individual node degree background weights
d_weights2={}
with open('karate_weightsIndDegree.txt', 'r') as weights:
    for line in weights:
        vals = line.split()
        d_weights2[(int(vals[0]), int(vals[1]))]=float(vals[2])
        d_weights2[(int(vals[1]), int(vals[0]))]=float(vals[2])

_, f2 = SteinerTree(G, query, 6, d_weights2, len(G.nodes()))
edgelist = [(k, v) for k, v in f2.items() if v != None]
edgelistRemaining = [(k, v) for k, v in G.edges() if (k, v) not in edgelist and (v,k) not in edgelist]

plt.figure(3)
plt.axis('off')
axes = plt.gca()
axes.set_xlim([xmin,xmax])
axes.set_ylim([ymin,ymax])
nx.draw_networkx_nodes(G, pos, nodelist = c1, node_color = 'r')
nx.draw_networkx_nodes(G, pos, nodelist = c2, node_color = 'g')
nx.draw_networkx_edges(G, pos, edgelist, width=2)
nx.draw_networkx_edges(G, pos, edgelistRemaining, alpha = 0.2)
#nx.draw_networkx_labels(G, pos)
plt.savefig("C:\\Users\\fawadria\\Desktop\\Journal DAMI git\\KarateIndPrior.pdf", bbox_inches='tight',pad_inches = 0, close=True)

## Plotting Forest with Community density prior
d_weights3={}
with open('karate_bins_Community_weights.txt', 'r') as weights:
    for line in weights:
        vals = line.split()
        d_weights3[(int(vals[0]), int(vals[1]))]=float(vals[2])
        d_weights3[(int(vals[1]), int(vals[0]))]=float(vals[2])

_, f3 = SteinerTree(G, query, 6, d_weights3, len(G.nodes()))
edgelist = [(k, v) for k, v in f3.items() if v != None]
edgelistRemaining = [(k, v) for k, v in G.edges() if (k, v) not in edgelist and (v,k) not in edgelist]

plt.figure(4)
plt.axis('off')
axes = plt.gca()
axes.set_xlim([xmin,xmax])
axes.set_ylim([ymin,ymax])
nx.draw_networkx_nodes(G, pos, nodelist = c1, node_color = 'r')
nx.draw_networkx_nodes(G, pos, nodelist = c2, node_color = 'g')
nx.draw_networkx_edges(G, pos, edgelist, width=2)
nx.draw_networkx_edges(G, pos, edgelistRemaining, alpha = 0.2)
#nx.draw_networkx_labels(G, pos)
plt.savefig("C:\\Users\\fawadria\\Desktop\\Journal DAMI git\\KarateCommunityPrior.pdf", bbox_inches='tight',pad_inches = 0, close=True)

# The tree found by Akoglu's method, with query = c2.
TreeAkoglu_c2 = {'29':'32', '32':'25', '28':'25', '25': '26', '26':'24', '24':'30', '30':'27', '27':'34', '10':'34', '34':'15', '15': '33', '9':'31', '31':'33', '21': '33', '16': '33', '19':'33', '23':'33', '33': None}
edgelist = [(k, v) for k, v in TreeAkoglu_c2.items() if v != None]
edgelistRemaining = [(k, v) for k, v in G.edges() if (k, v) not in edgelist and (v,k) not in edgelist]

plt.figure(5)
plt.axis('off')
axes = plt.gca()
axes.set_xlim([xmin,xmax])
axes.set_ylim([ymin,ymax])
nx.draw_networkx_nodes(G, pos, nodelist = c1, node_color = 'r')
nx.draw_networkx_nodes(G, pos, nodelist = c2, node_color = 'g')
nx.draw_networkx_edges(G, pos, edgelist, width=2)
nx.draw_networkx_edges(G, pos, edgelistRemaining, alpha = 0.2)
#nx.draw_networkx_labels(G, pos)
plt.savefig("C:\\Users\\fawadria\\Desktop\\Journal DAMI git\\KarateLeman.pdf", bbox_inches='tight',pad_inches = 0, close=True)
#plt.show()
