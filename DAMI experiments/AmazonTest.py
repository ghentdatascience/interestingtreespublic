from Functions import *

# The script for running tests on the (undirected) Amazondata

# G is directed graph (for each undirected edge two directions)
with open('AmazonGraph.pickle', 'rb') as handle:
     G = pickle.load(handle)

# d_weights is dict with edge weights.
with open('AmazonWeightsIndDegree.pickle', 'rb') as handle:
    d_weights= pickle.load(handle)

#Performance(G, G, 500, 4, 4, 0.8, 4, d_weights, 'PerformanceAmazonQ4.txt', 'TimingAmazonQ4.txt')
#Performance(G, G, 500, 8, 4, 0.8, 4, d_weights, 'PerformanceAmazonQ8.txt', 'TimingAmazonQ8.txt')
#Performance(G, G, 500, 12, 4, 0.8, 4, d_weights, 'PerformanceAmazonQ12.txt', 'TimingAmazonQ12.txt')
