from Functions import *

# Akoglu DBLP query (KDD vs. RECOMB)
query = {'101711', '11235', '99749', '12451', '52167', '11331', '20658', '101697', '42241', '42103', '32970', '38144', '87220', '12119', '5959', '13440', '87117', '26626', '13408', '65017'}
tree_depth = 15
V = len(G.nodes())

nohup python -u kddvsrecomb.py > kddvsrecomb.log &

with open('DBLP_AA_GCC_Authors.txt', 'r') as weights:
	teller=1
	for line in weights:
		d[teller]=line.split()[0]
		teller+=1