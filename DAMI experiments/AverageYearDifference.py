from Functions import *

with open('ACMGraph.pickle', 'rb') as handle:
     G = pickle.load(handle)
with open('ACMGraphRev.pickle', 'rb') as handle:
     Grev = pickle.load(handle)
# d_weights is dict with edge weights, for prior = bins.
with open('ACMWeightsBins.pickle', 'rb') as handle:
    d_weights= pickle.load(handle)

G.remove_nodes_from(['1445034', '1539934', '1659030', '1696259', '1750721']) # nodes with no year label
Grev.remove_nodes_from(['1445034', '1539934', '1659030', '1696259', '1750721']) # nodes with no year label

# Loading d_meta to get the year info
with open('ACM_dmeta.pickle', 'rb') as handle:
    d_meta= pickle.load(handle)

# Loading the d_weightsNorma, for prior = ind. degree
d_weightsNormal = {}
with open('ACMWeightsDegree.pickle', 'rb') as handle:
    d_weightsNormal= pickle.load(handle)

teller = 0
tellerEqual = 0
numberoftrees = 3
querysize = 8
beamwidth = 3
s = 0.4
tree_depth = 8
V = len(G.nodes())

with open('ACM_TestingYearDifference.txt', 'a') as fileYear:
    while teller<numberoftrees:
        init_node = random.choice(G.nodes())
        query = GenerateQuerySet(G, init_node, querysize, beamwidth, s)
        commonRoots, pruned_space = Common(G, query, tree_depth)
        if len(query) == querysize and len(commonRoots)>0:
            n1, B1 = SteinerBranching(G, Grev, query, tree_depth, d_weights, V)
            n2, B2 = SteinerBranching(G, Grev, query, tree_depth, d_weightsNormal, V)
            teller+=1
            if B1 != B2:
                avgYearB1 = 0
                for i in B1:
                    if B1[i]!=None:
                        avgYearB1 += int(d_meta[int(i)][2])-int(d_meta[int(B1[i])][2])
                avgYearB1 = avgYearB1/(len(B1)-n1)
                avgYearB2 = 0
                for i in B2:
                    if B2[i]!=None:
                        avgYearB2 += int(d_meta[int(i)][2])-int(d_meta[int(B2[i])][2])
                avgYearB2 = avgYearB2/(len(B2)-n2)
                fileYear.write(repr(avgYearB1)+'\t'+repr(avgYearB2)+'\n')
            else:
                tellerEqual+=1
    fileYear.write(repr(tellerEqual))
