from Functions import *

# The script for running tests on the (undirected) Amazondata

# G is directed graph (for each undirected edge two directions)
with open('ACMGraph.pickle', 'rb') as handle:
     G = pickle.load(handle)
with open('ACMGraphRev.pickle', 'rb') as handle:
     Grev = pickle.load(handle)

# d_weights is dict with edge weights.
with open('ACMWeightsBins.pickle', 'rb') as handle:
    d_weights= pickle.load(handle)

#Performance(G, Grev, 500, 4, 4, 0.8, 4, d_weights, 'PerformanceACMQ4.txt', 'TimingACMQ4.txt')
#Performance(G, Grev, 500, 8, 4, 0.8, 4, d_weights, 'PerformanceACMQ8.txt', 'TimingACMQ8.txt')
#Performance(G, Grev, 500, 12, 4, 0.8, 4, d_weights, 'PerformanceACMQ12.txt', 'TimingACMQ12.txt')
