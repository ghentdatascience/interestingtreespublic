from Functions import *

### Calculating the average degree of nodes in a network vs nodes in our trees (Amazon data) ###

with open('AmazonGraph.pickle', 'rb') as handle:
     G = pickle.load(handle)

with open('AmazonWeightsIndDegree.pickle', 'rb') as handle:
    d_weights= pickle.load(handle)

d_weightsNormal = {}
for e in G.edges():
    d_weightsNormal[(int(e[0]),int(e[1]))] = 0.8

# Average degree of (constant prior vs ind. degree prior) over 200 trees with Querysize 8, depth 8.
teller = 0
tellerEqual = 0
numberoftrees = 10
querysize = 8
beamwidth = 3
s = 0.4
tree_depth = 8
V = len(G.nodes())
with open('Amazon_TestingAverageDegree.txt', 'a') as fileDegree:
    while teller < numberoftrees:
        init_node = random.choice(G.nodes())
        query = GenerateQuerySet(G, init_node, querysize, beamwidth, s)
        commonRoots, pruned_space = Common(G, query, tree_depth)
        if len(query) == querysize and len(commonRoots)>0:
            random_root = random.choice(list(commonRoots))
            Tree1 = SteinerForest(G, query, tree_depth, d_weights, V)[1]
            Tree2 = SteinerForest(G, query, tree_depth, d_weightsNormal, V)[1]
            somD1 = 0
            for n in Tree1:
                somD1 += G.degree(n)/2
            somD2 = 0
            for n in Tree2:
                somD2 += G.degree(n)/2
            if somD1 != 0 and somD2 != 0:
                averageD1 = somD1/len(Tree1)
                averageD2 = somD2/len(Tree2)
                teller+=1
                if averageD1 != averageD2:
                    fileDegree.write(repr(averageD2)+'\t'+repr(averageD1)+'\n')
                else:
                    tellerEqual += 1
            
    fileDegree.write(repr(tellerEqual))
