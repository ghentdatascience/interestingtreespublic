from matplotlib import pyplot as plt
from Functions import *
from numpy import array

# G1: forest with neutral beliefs.
# G2: forest with individual degree prior.
# G3: forest from akoglu

#####
G1 = nx.Graph()
G1.add_edge('Bernard_Schlkopf','Patrice_Simard')
G1.add_edge('Yann_LeCun','Patrice_Simard')
G1.add_edge('Yann_LeCun','Yoshua_Bengio')
G1.add_edge('Yann_LeCun','Geoffrey_E._Hinton')
G1.add_edge('Geoffrey_E._Hinton','Michael_I._Jordan')
G1.add_edge('Satinder_P._Singh','Michael_I._Jordan')
G1.add_edge('Geoffrey_E._Hinton','Christopher_K._I._Williams')
G1.add_edge('Michael_Mozer','Christopher_K._I._Williams')
G1.add_edge('Zoubin_Ghahramani','Michael_I._Jordan')

G1.add_edge('J._D._Ullman', 'C._Papadimitriou')
G1.add_edge('Georg_Gottlob', 'C._Papadimitriou')
G1.add_edge('Georg_Gottlob', 'Thomas_Schwentick')
G1.add_edge('Leonid_Libkin', 'Thomas_Schwentick')
G1.add_edge('Frank_Neven', 'Thomas_Schwentick')
G1.add_edge('Frank_Neven', 'Dirk_Van_Gucht')
G1.add_edge('Frank_Neven', 'Tova_Milo')
########
G2 = nx.Graph()
G2.add_edge('Yoshua_Bengio', 'Franois_Rivest')
G2.add_edge('Franois_Rivest','Doina_Precup')
G2.add_edge('Doina_Precup','Satinder_P._Singh')
G2.add_edge('Satinder_P._Singh', 'Lawrence_K._Saul')
G2.add_edge('Lawrence_K._Saul', 'Geoffrey_E._Hinton')
G2.add_edge('Lawrence_K._Saul', 'Daniel_D._Lee')
G2.add_edge('Daniel_D._Lee', 'Bernard_Schlkopf')
G2.add_edge('Geoffrey_E._Hinton','Michael_Mozer')
G2.add_edge('Geoffrey_E._Hinton','Zoubin_Ghahramani')

G2.add_edge('Dirk_Van_Gucht','Frank_Neven')
G2.add_edge('Frank_Neven','Tova_Milo')
G2.add_edge('Frank_Neven','Leonid_Libkin')
G2.add_edge('Leonid_Libkin','Martin_Grohe')
G2.add_edge('Leonid_Libkin','Howard_Trickey')
G2.add_edge('Howard_Trickey','J._D._Ullman')
G2.add_edge('Martin_Grohe','Georg_Gottlob')

######
G3 = nx.Graph()
G3.add_edge('Yoshua_Bengio','Yann_LeCun')
G3.add_edge('Lawrence_K._Saul','Yann_LeCun')
G3.add_edge('Lawrence_K._Saul','Zoubin_Ghahramani')
G3.add_edge('Lawrence_K._Saul','Geoffrey_E._Hinton')
G3.add_edge('Michael_Mozer','Geoffrey_E._Hinton')
G3.add_edge('Lawrence_K._Saul','Satinder_P._Singh')
G3.add_edge('Lawrence_K._Saul','Daniel_D._Lee')
G3.add_edge('Bernard_Schlkopf','Daniel_D._Lee')

G3.add_edge('Dirk_Van_Gucht','Frank_Neven')
G3.add_edge('Frank_Neven','Tova_Milo')
G3.add_edge('Frank_Neven','Thomas_Schwentick')
G3.add_edge('Georg_Gottlob','Thomas_Schwentick')
G3.add_edge('Leonid_Libkin','Frank_Neven')
G3.add_edge('Howard_Trickey','Leonid_Libkin')
G3.add_edge('Howard_Trickey','J._D._Ullman')

######

plt.ioff()
xmin = -0.55
xmax = 1.58
ymin = -0.13
ymax = 0.918
plt.figure(1)
plt.axis('off')
axes = plt.gca()
axes.set_xlim([xmin,xmax])
axes.set_ylim([ymin,ymax])
pos = {'Michael_Mozer': array([ 0.42,  0.7025158]), 'Georg_Gottlob': array([ 0.7219861,  0.1010879]), 'Zoubin_Ghahramani': array([ 0.37996911,  0.8361404]),
       'Yoshua_Bengio': array([ 0.2927091 ,  0.25827704]), 'Christopher_K._I._Williams': array([ 0.3,  0.60439402]), 'Michael_I._Jordan': array([ 0.1735907,  0.70843972]),
       'Yann_LeCun': array([ 0.15459639,  0.4026296]), 'Patrice_Simard': array([ 0.02114023,  0.27639907]), 'Leonid_Libkin': array([ 1.1474487,  0.22093365]),
       'Thomas_Schwentick': array([ 0.74415754,  0.3053415]), 'Bernard_Schlkopf': array([ 0.        ,  0.11504305]), 'Frank_Neven': array([ 1.0548367,  0.48508749]),
       'C._Papadimitriou': array([ 0.57136295,  -0.00506615]), 'J._D._Ullman': array([ 0.43267059,  -0.1        ]), 'Tova_Milo': array([ 0.8        ,  0.47876157]),
       'Dirk_Van_Gucht': array([ 1.3038106,  0.65499949]), 'Satinder_P._Singh': array([ 0.06649712,  0.83169465]), 'Geoffrey_E._Hinton': array([ 0.1976806 ,  0.55157336])}

poslabels = {'Michael_Mozer': array([ 0.58,  0.7525158]), 'Georg_Gottlob': array([ 1.039861,  0.07610879]), 'Zoubin_Ghahramani': array([ 0.6356911,  0.8911404]),
       'Yoshua_Bengio': array([ 0.3227091 ,  0.19827704]), 'Christopher_K._I._Williams': array([ 0.76,  0.60439402]), 'Michael_I._Jordan': array([ -0.16835907,  0.70843972]),
       'Yann_LeCun': array([ -0.12459639,  0.4026296]), 'Patrice_Simard': array([ -0.28,  0.27639907]), 'Leonid_Libkin': array([ 1.3274487,  0.155093365]),
       'Thomas_Schwentick': array([ 1.14754,  0.3153415]), 'Bernard_Schlkopf': array([ -.15        ,  0.0504305]), 'Frank_Neven': array([ 1.29848367,  0.42508749]),
       'C._Papadimitriou': array([ 0.90136295,  -0.0406615]), 'J._D._Ullman': array([ 0.153267059,  -0.1        ]), 'Tova_Milo': array([ 0.7        ,  0.41876157]),
       'Dirk_Van_Gucht': array([ 1.30038106,  0.7099949]), 'Satinder_P._Singh': array([ -0.1,  0.89169465]), 'Geoffrey_E._Hinton': array([ -0.17 ,  0.55157336])}

labels = {'Frank_Neven': 'Frank_Neven [21]', 'J._D._Ullman': 'J._D._Ullman [96]',
          'C._Papadimitriou': 'C._Papadimitriou [102]', 'Geoffrey_E._Hinton': 'Geoffrey_E._Hinton [44]', 'Michael_I._Jordan': 'Michael_I._Jordan [91]',
          'Michael_Mozer': 'Michael_Mozer [32]', 'Bernard_Schlkopf': 'Bernard_Schlkopf [99]', 'Yann_LeCun': 'Yann_LeCun [52]',
          'Thomas_Schwentick': 'Thomas_Schwentick [33]', 'Tova_Milo': 'Tova_Milo [69]', 'Satinder_P._Singh': 'Satinder_P._Singh [51]',
          'Yoshua_Bengio': 'Yoshua_Bengio [47]', 'Christopher_K._I._Williams': 'Christopher_K._I._Williams [76]', 'Leonid_Libkin': 'Leonid_Libkin [33]',
          'Dirk_Van_Gucht': 'Dirk_Van_Gucht [36]',
          'Patrice_Simard': 'Patrice_Simard [35]', 'Georg_Gottlob': 'Georg_Gottlob [90]', 'Zoubin_Ghahramani': 'Zoubin_Ghahramani [53]'}

query = ['J._D._Ullman','Michael_Mozer','Bernard_Schlkopf','Yoshua_Bengio','Tova_Milo','Satinder_P._Singh', 'Leonid_Libkin','Dirk_Van_Gucht', 'Georg_Gottlob','Zoubin_Ghahramani']  
other = set(G1.nodes())-set(query)
nx.draw_networkx_edges(G1, pos, alpha = 0.2)
nx.draw_networkx_nodes(G1, pos, query, node_shape = 's', node_size = 200, linewidths=0)
nx.draw_networkx_nodes(G1, pos, other, node_shape = 'o', node_size = 50, linewidths=0, node_color = 'k')
nx.draw_networkx_labels(G1, labels = labels, pos=poslabels, font_size = 10, font_weight='bold')

# Drawing a fill inside cornerpoints of a polygon.
x = [-0.155, 0.06, 0.377, 0.55, 0.467, -0.069, 0.017]
y = [0.0937, 0.02, 0.224, 0.67, 0.874, 0.86, 0.49]
plt.fill(x,y,'b',alpha = 0.1)
x = [0.36, 0.493, 1.266, 1.425, 1.224, 0.652, 0.558]
y = [-0.122, -0.134, 0.184, 0.67, 0.743, 0.465, 0.102]
plt.fill(x,y,'r',alpha=0.1)
plt.savefig("C:\\Users\\fawadria\\Desktop\\Journal DAMI git\\DBLP_Neutral.pdf", bbox_inches='tight',pad_inches = 0, close=True)

plt.figure(2)
plt.axis('off')
ymin = -1.12
ymax = 1.42
axes = plt.gca()
axes.set_ylim([ymin,ymax])
xshift = 1.85
pos = {'Leonid_Libkin': array([-1.05688705+xshift, -0.18198841]), 'Franois_Rivest': array([ 0.77372812-xshift+0.3,  -0.25]), 'Martin_Grohe': array([-0.7540317+xshift-0.2 , -0.49770276]),
       'Bernard_Schlkopf': array([ 0.-xshift        ,  -0.2]),
     'Frank_Neven': array([-1.259552+xshift+0.2  ,  0.24856842]), 'Doina_Precup': array([ 1.15997831-xshift,  0.0921806 ]), 'Yoshua_Bengio': array([ 0.27091-xshift+0.4   ,  -0.7]),
       'Tova_Milo': array([-1.56181909+xshift+0.35,  0.78162192]),
     'Lawrence_K._Saul': array([ 0.79352999-xshift,  0.68323424]), 'Geoffrey_E._Hinton': array([ 0.83745249-xshift,  1.0308271 ]), 'Zoubin_Ghahramani': array([ 1.27077272-xshift,  1.23984668]),
       'Daniel_D._Lee': array([ 0.38399051-xshift,  0.39823897]),
     'Dirk_Van_Gucht': array([-1.29932869+xshift+0.5+0.1,  0.84183744-0.3]), 'Georg_Gottlob': array([-0.49025913+xshift, -0.93373113]), 'Michael_Mozer': array([ 0.37996911-xshift,  1.2361404 ]),
     'Howard_Trickey': array([-1.04061575+xshift-0.2, -0.63205921]), 'J._D._Ullman': array([-0.76694217+xshift-1, -0.93366533]), 'Satinder_P._Singh': array([ 1.15717651-xshift-0.1,  0.35206976])}

poslabels = {'Leonid_Libkin': array([-1.05688705+xshift+0.625, -0.18198841]), 'Franois_Rivest': array([ 0.77372812-xshift+0.88,  -0.25]), 'Martin_Grohe': array([-0.7540317+xshift+0.38 , -0.49770276]),
       'Bernard_Schlkopf': array([ 0.-xshift        ,  -0.35]),
     'Frank_Neven': array([-1.259552+xshift+0.2+0.58  ,  0.24856842]), 'Doina_Precup': array([ 1.15997831-xshift+0.6,  0.1121806 ]), 'Yoshua_Bengio': array([ 0.27091-xshift+0.15   ,  -0.86]),
       'Tova_Milo': array([-1.56181909+xshift+0.1+0.32,  0.78162192+0.11]),
     'Lawrence_K._Saul': array([ 0.79352999-xshift+0.72,  0.68323424]), 'Geoffrey_E._Hinton': array([ 0.83745249-xshift-0.74,  1.0308271-0.05 ]), 'Zoubin_Ghahramani': array([ 1.27077272-xshift+0.3,  1.23984668+0.12]),
       'Daniel_D._Lee': array([ 0.38399051-xshift-0.5,  0.47823897]),
     'Dirk_Van_Gucht': array([-1.29932869+xshift+0.5+0.32,  0.84183744-0.18]), 'Georg_Gottlob': array([-0.49025913+xshift, -0.93373113-0.15]), 'Michael_Mozer': array([ 0.37996911-xshift-0.3,  1.2361404+0.12 ]),
     'Howard_Trickey': array([-1.04061575+xshift-0.85, -0.63205921]), 'J._D._Ullman': array([-0.76694217+xshift-1, -0.93366533-0.15]), 'Satinder_P._Singh': array([ 1.15717651-xshift+0.68,  0.35206976])}


labels = {'Frank_Neven': 'Frank_Neven [21]', 'J._D._Ullman': 'J._D._Ullman [96]',
          'Franois_Rivest': 'Franois_Rivest [4]', 'Geoffrey_E._Hinton': 'Geoffrey_E._Hinton [44]',
          'Michael_Mozer': 'Michael_Mozer [32]', 'Bernard_Schlkopf': 'Bernard_Schlkopf [99]',
          'Tova_Milo': 'Tova_Milo [69]', 'Satinder_P._Singh': 'Satinder_P._Singh [51]',
          'Yoshua_Bengio': 'Yoshua_Bengio [47]', 'Leonid_Libkin': 'Leonid_Libkin [33]',
          'Dirk_Van_Gucht': 'Dirk_Van_Gucht [36]', 'Daniel_D._Lee': 'Daniel_D._Lee [18]',
          'Doina_Precup': 'Doina_Precup [44]', 'Lawrence_K._Saul': 'Lawrence_K._Saul [19]',
          'Martin_Grohe': 'Martin_Grohe [32]', 'Howard_Trickey': 'Howard_Trickey [9]',
          'Georg_Gottlob': 'Georg_Gottlob [90]', 'Zoubin_Ghahramani': 'Zoubin_Ghahramani [53]'}

other = set(G2.nodes())-set(query)
nx.draw_networkx_edges(G2, pos, alpha = 0.2)
nx.draw_networkx_nodes(G2, pos, query, node_shape = 's', node_size = 200, linewidths=0)
nx.draw_networkx_nodes(G2, pos, other, node_shape = 'o', node_size = 50, linewidths=0, node_color = 'k')
nx.draw_networkx_labels(G2, labels = labels, pos=poslabels, font_size = 10, font_weight='bold')

# Drawing a fill inside cornerpoints of a polygon.
x = [-1.166, -0.56, -0.458, -0.667, -0.2409, -1.73, -1.56, -2.11]
y = [-0.901, -0.32, 0.123, 0.79, 1.33, 1.32, 0.72, -0.299]
plt.fill(x,y,'b',alpha = 0.1)
x = [-0.132, 1.5645, 1.056, 1.446, 0.43, 0.53, 0.13]
y = [-1.067, -1.014, -0.12, 0.59, 0.97, -0.14, -0.696]
plt.fill(x,y,'r',alpha=0.1)
plt.savefig("C:\\Users\\fawadria\\Desktop\\Journal DAMI git\\DBLP_Degree.pdf", bbox_inches='tight',pad_inches = 0, close=True)

plt.figure(3)
plt.axis('off')
xmin = -2.8
xmax = 2.65
ymin = -1.12
ymax = 1.67
axes = plt.gca()
axes.set_ylim([ymin,ymax])
axes.set_xlim([xmin,xmax])
xshift = 1.85

pos = {'Leonid_Libkin': array([-1.05688705+xshift-0.2, -0.26198841]), 'Yann_LeCun': array([ 0.77372812-xshift,  -0.12]), 'Thomas_Schwentick': array([-0.7540317+xshift-0.1-0.1 , -0.49770276]),
       'Bernard_Schlkopf': array([ 0.-xshift-0.15        ,  0]),
     'Frank_Neven': array([-1.259552+xshift+0.4  ,  0.24856842]), 'Yoshua_Bengio': array([ 0.27091-xshift   ,  -0.7]),
       'Tova_Milo': array([-1.56181909+xshift+0.35,  0.78162192]),
     'Lawrence_K._Saul': array([ 0.79352999-xshift,  0.68323424]), 'Geoffrey_E._Hinton': array([ 0.83745249-xshift,  1.0308271 ]), 'Zoubin_Ghahramani': array([ 1.27077272-xshift+0.45,  1.23984668+0.1]),
       'Daniel_D._Lee': array([ 0.38399051-xshift+0.15,  0.39823897]),
     'Dirk_Van_Gucht': array([-1.29932869+xshift+0.5+0.4,  0.84183744-0.26]), 'Georg_Gottlob': array([-0.49025913+xshift, -0.93373113]), 'Michael_Mozer': array([ 0.37996911-xshift+0.4,  1.2361404+0.2 ]),
     'Howard_Trickey': array([-1.04061575+xshift-0.2-0.2, -0.63205921]), 'J._D._Ullman': array([-0.76694217+xshift-1, -0.93366533]), 'Satinder_P._Singh': array([ 1.15717651-xshift+0.25,  0.35206976])}

labels = {'Frank_Neven': 'Frank_Neven [21]', 'J._D._Ullman': 'J._D._Ullman [96]',
          'Yann_LeCun': 'Yann_LeCun [52]', 'Geoffrey_E._Hinton': 'Geoffrey_E._Hinton [44]',
          'Michael_Mozer': 'Michael_Mozer [32]', 'Bernard_Schlkopf': 'Bernard_Schlkopf [99]',
          'Tova_Milo': 'Tova_Milo [69]', 'Satinder_P._Singh': 'Satinder_P._Singh [51]',
          'Yoshua_Bengio': 'Yoshua_Bengio [47]', 'Leonid_Libkin': 'Leonid_Libkin [33]',
          'Dirk_Van_Gucht': 'Dirk_Van_Gucht [36]', 'Daniel_D._Lee': 'Daniel_D._Lee [18]',
          'Lawrence_K._Saul': 'Lawrence_K._Saul [19]',
          'Thomas_Schwentick': 'Thomas_Schwentick [33]', 'Howard_Trickey': 'Howard_Trickey [9]',
          'Georg_Gottlob': 'Georg_Gottlob [90]', 'Zoubin_Ghahramani': 'Zoubin_Ghahramani [53]'}

poslabels = {'Leonid_Libkin': array([-1.05688705+xshift-0.82-0.12, -0.26198841]), 'Yann_LeCun': array([ 0.77372812-xshift+0.65,  -0.08]), 'Thomas_Schwentick': array([-0.7540317+xshift-0.1+0.82 , -0.49770276]),
       'Bernard_Schlkopf': array([ 0.-xshift -0.15       ,  -0.15]),
     'Frank_Neven': array([-1.259552+xshift+0.5+0.62  ,  0.24856842]), 'Yoshua_Bengio': array([ 0.27091-xshift   ,  -0.87]),
       'Tova_Milo': array([-1.56181909+xshift+0.35+0.05,  0.78162192+0.15]),
     'Lawrence_K._Saul': array([ 0.79352999-xshift-0.72-0.15,  0.68323424]), 'Geoffrey_E._Hinton': array([ 0.83745249-xshift-0.75-0.15,  1.0308271 ]), 'Zoubin_Ghahramani': array([ 1.27077272-xshift+0.72+0.3,  1.23984668+0.23]),
       'Daniel_D._Lee': array([ 0.38399051-xshift-0.44-0.14,  0.39823897]),
     'Dirk_Van_Gucht': array([-1.29932869+xshift+0.5+0.1+0.5,  0.84183744-0.13]), 'Georg_Gottlob': array([-0.49025913+xshift, -1.093373113]), 'Michael_Mozer': array([ 0.37996911-xshift+0.3-0.1,  1.2361404+0.34]),
     'Howard_Trickey': array([-1.04061575+xshift-0.2-0.82-0.1, -0.63205921+0.05]), 'J._D._Ullman': array([-0.76694217+xshift-1.1, -1.08366533]), 'Satinder_P._Singh': array([ 1.15717651-xshift-0.1+0.55,  0.35206976-0.13])}


other = set(G3.nodes())-set(query)
nx.draw_networkx_edges(G3, pos, alpha = 0.2)
nx.draw_networkx_nodes(G3, pos, query, node_shape = 's', node_size = 200, linewidths=0)
nx.draw_networkx_nodes(G3, pos, other, node_shape = 'o', node_size = 50, linewidths=0, node_color = 'k')
nx.draw_networkx_labels(G3, labels = labels, pos=poslabels, font_size = 10, font_weight='bold')

# Drawing a fill inside cornerpoints of a polygon.
x = [-1.58, -0.15, 0.098, -1.38, -1.49, -2.38]
y = [-0.89, 0.29, 1.4, 1.57, 0.74, 0.013]
plt.fill(x,y,'b',alpha = 0.1)
x = [-0.228, 1.64, 1.23, 1.82, 0.48, 0.25, 0.57, 0.156]
y = [-1.03, -1.025, -0.132, 0.67, 1.001, 0.72, 0.166, -0.59]
plt.fill(x,y,'r',alpha=0.1)
plt.savefig("C:\\Users\\fawadria\\Desktop\\Journal DAMI git\\DBLP_Akoglu.pdf", bbox_inches='tight',pad_inches = 0, close=True)

plt.show()
