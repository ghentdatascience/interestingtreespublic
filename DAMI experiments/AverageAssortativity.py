from Functions import *

with open('DBLPGraph.pickle', 'rb') as handle:
     G = pickle.load(handle)

with open('DBLPWeightsAssortivity.pickle', 'rb') as handle:
    d_weights= pickle.load(handle)

with open('DBLPWeightsIndDegree.pickle', 'rb') as handle:
    d_weightsInD= pickle.load(handle)

teller = 0
tellerEqual = 0
numberoftrees = 30
querysize = 6
beamwidth = 3
s = 0.4
tree_depth = 6
V = len(G.nodes())

with open('DBLP_TestAssort.txt','a') as fileA:
    while teller<numberoftrees:
        init_node = random.choice(G.nodes())
        query = GenerateQuerySet(G, init_node, querysize, beamwidth, s)
        if len(query) == querysize:
            check1, T1 = SteinerTree(G, query, tree_depth, d_weights, V)
            check2, T2 = SteinerTree(G, query, tree_depth, d_weightsInD, V)
            teller+=1
            if check1 and check2 and T1 != T2:
                sumT1 = 0
                for i in T1:
                    if T1[i] != None:
                        sumT1 += abs(G.degree(i)-G.degree(T1[i]))
                sumT1 = sumT1/(len(T1)-1)
                sumT2 = 0
                for i in T2:
                    if T2[i]!=None:
                        sumT2 += abs(G.degree(i)-G.degree(T2[i]))
                sumT2 = sumT2/(len(T2)-1)
                fileA.write(repr(sumT1)+'\t'+repr(sumT2)+'\n')
            else:
                tellerEqual+=1
    fileA.write(repr(tellerEqual))
