from Functions import *

# The script for running tests on the (undirected) Amazondata

# G is directed graph (for each undirected edge two directions)
with open('AmazonGraph.pickle', 'rb') as handle:
     G = pickle.load(handle)

# d_weights is dict with edge weights.
with open('AmazonWeightsIndDegree.pickle', 'rb') as handle:
    d_weights= pickle.load(handle)


Speed(G,G,50,14,10,0.8,12,d_weights,'TimingTest12.txt')
Speed(G,G,50,14,10,0.8,14,d_weights,'TimingTest14.txt')
