from Functions import *
from itertools import combinations

#######################################################################################################################	
######## Reading the(undirected) Amazon dataset & weighted dict. of edges and storing it as a pickle dump ########
#######################################################################################################################	

# Creating the graph and storing it as a pickle file.
# For undirected graphs: read as a DiGraph, take the reverse Graph and combine the 2 graphs into one graph.
G = nx.read_edgelist("com-amazon.ungraph.txt", create_using=nx.DiGraph())
Grev = nx.reverse(G)
G = nx.compose(G,Grev)

with open('AmazonGraph.pickle', 'wb') as handle:
    pickle.dump(G, handle, protocol=pickle.HIGHEST_PROTOCOL)

    
# Reading the weight dict and storing it as a pickle file.
d_weights={}
with open('Amazon_weights_IndDegree.txt', 'r') as weights:
	for line in weights:
		vals = line.split()
		d_weights[(int(vals[0]), int(vals[1]))]=float(vals[2])
		d_weights[(int(vals[1]), int(vals[0]))]=float(vals[2])
with open('AmazonWeightsIndDegree.pickle', 'wb') as handle:
    pickle.dump(d_weights, handle, protocol=pickle.HIGHEST_PROTOCOL)

#######################################################################################################################	
######## Reading the(undirected) DBLP dataset & weighted dict of edges and storing as a pickle dump  ########
#######################################################################################################################
	
# Creating the graph and storing it as a pickle file.   
G = nx.read_edgelist("com-dblp.ungraph.txt", create_using=nx.DiGraph())
# For computing the degree assort. coefficient, use:
# G = nx.read_edgelist("com-dblp.ungraph.txt")
# c = nx.degree_assortativity_coefficient(G)
Grev = nx.reverse(G)
G = nx.compose(G,Grev)

with open('DBLPGraph.pickle', 'wb') as handle:
    pickle.dump(G, handle, protocol=pickle.HIGHEST_PROTOCOL)

# Reading the weight dict and storing it as a pickle file.
d_weights={}
with open('DBLP_weights_Assortivity.txt', 'r') as weights:
	for line in weights:
		vals = line.split()
		d_weights[(int(vals[0]), int(vals[1]))]=float(vals[2])
		d_weights[(int(vals[1]), int(vals[0]))]=float(vals[2])
with open('DBLPWeightsAssortivity.pickle', 'wb') as handle:
    pickle.dump(d_weights, handle, protocol=pickle.HIGHEST_PROTOCOL)

#######################################################################################################################	
######## ToyExample reading the(undirected) ACM dataset & weighted dict of edges and storing as a pickle dump ########
#######################################################################################################################	
G = nx.read_edgelist("acm-connectivity.txt", create_using=nx.DiGraph())
Grev = nx.reverse(G)	

with open('ACMGraph.pickle', 'wb') as handle:
    pickle.dump(G, handle, protocol=pickle.HIGHEST_PROTOCOL)
	
with open('ACMGraphRev.pickle', 'wb') as handle:
    pickle.dump(Grev, handle, protocol=pickle.HIGHEST_PROTOCOL)
	
# Reading the edge weight dict. and storing it as a pickle file

d_weights={}
with open('acm-weightsDAG_bin5.txt', 'r') as weights:
	for line in weights:
		vals = line.split()
		d_weights[(int(vals[0]), int(vals[1]))]=float(vals[2])

with open('ACMWeightsBins.pickle', 'wb') as handle:
    pickle.dump(d_weights, handle, protocol=pickle.HIGHEST_PROTOCOL)

# Reading the meta data and storing it as a dict.

d_meta={}
d_index={} 
with open('citation-acm-v8.txt', 'r', encoding = 'utf8') as meta:
    teller = 0
    authors = []
    for line in meta:
        #print(line, end='')
        if '#*' in line:
            title = line[2:-1]
        elif '#@' in line:
            authors = line[2:-1]
        elif '#t' in line:
            year = line[2:-1] 
        elif '#index' in line:
            index_value = line[6:-1]
            d_index[index_value]=teller
            ref = []
        elif '#%' in line:
            ref.append(line[2:-1])
        elif line == '\n':
            d_meta[teller] = [title, authors, year, ref]
            teller += 1

with open('ACM_dmeta.pickle', 'wb') as handle:
    pickle.dump(d_meta, handle, protocol=pickle.HIGHEST_PROTOCOL)

d_weightsIndDegree={}
with open('acm-weights.txt', 'r') as weights:
	for line in weights:
		vals = line.split("\t")
		d_weightsIndDegree[(int(vals[0]), int(vals[1]))]=float(vals[2])
with open('ACMWeightsDegree.pickle', 'wb') as handle:
    pickle.dump(d_weightsIndDegree, handle, protocol=pickle.HIGHEST_PROTOCOL)
	
###################################################################################################################
###### Toyexample converting the ACM citation network to a DBLP coauthorship graph & storing the pickle dumps #####
###################################################################################################################

G = nx.Graph()
with open('citation-acm-v8.txt', 'r', encoding = 'utf8') as meta:
    authors = []
    authorsFinal = set()
    for line in meta:
        if '#@' in line:
            authors = line[2:-1]
            authors = [x.strip() for x in authors.split(',')]
            authorsFinal |= set(authors)
            
authorsFinal = sorted(authorsFinal)
d_AuthorstoID = {}
teller = 1
for i in authorsFinal:
    d_AuthorstoID[i]=teller
    teller+=1
	
with open('AuthorstoID.pickle', 'wb') as handle:
    pickle.dump(d_AuthorstoID, handle, protocol=pickle.HIGHEST_PROTOCOL)

with open('citation-acm-v8.txt', 'r', encoding = 'utf8') as meta:
    authors = []
    for line in meta:
        if '#@' in line:
            authors = line[2:-1]
            authors = [x.strip() for x in authors.split(',')]
            for i in combinations(authors, 2):
                G.add_edge(d_AuthorstoID[i[0]],d_AuthorstoID[i[1]])

# Removing self-edges from the graph G and making the resulting undirected graph directed in 2 ways, dumping it to a pickle file.

G.remove_edges_from([(i,i) for i in G.nodes()])
G = G.to_directed()

with open('ACMtoDBLPGraph.pickle', 'wb') as handle:
    pickle.dump(G, handle, protocol=pickle.HIGHEST_PROTOCOL)

## Assortative weights
d_weights={}
with open('ACMtoDBLP_weights_Assortivity.txt', 'r') as weights:
    for line in weights:
        vals = line.split()
        d_weights[(int(vals[0]), int(vals[1]))]=float(vals[2])
        d_weights[(int(vals[1]), int(vals[0]))]=float(vals[2])
        
with open('ACMtoDBLPWeightsAssortivity.pickle', 'wb') as handle:
    pickle.dump(d_weights, handle, protocol=pickle.HIGHEST_PROTOCOL)

## Ind. Degree weights
d_weights={}
with open('ACMtoDBLP_weights_IndDegree.txt', 'r') as weights:
    for line in weights:
        vals = line.split()
        d_weights[(int(vals[0]), int(vals[1]))]=float(vals[2])
        d_weights[(int(vals[1]), int(vals[0]))]=float(vals[2])

with open('ACMtoDBLPWeightsIndDegree.pickle', 'wb') as handle:
    pickle.dump(d_weights, handle, protocol=pickle.HIGHEST_PROTOCOL)

## Neutral weights
d_weights={}
with open('ACMtoDBLP_weights_IndDegree.txt', 'r') as weights:
    for line in weights:
        vals = line.split()
        d_weights[(int(vals[0]), int(vals[1]))]=0.2
        d_weights[(int(vals[1]), int(vals[0]))]=0.2

with open('ACMtoDBLPWeightsNeutral.pickle', 'wb') as handle:
    pickle.dump(d_weights, handle, protocol=pickle.HIGHEST_PROTOCOL)
