from Functions import *

# The script for running tests on the (undirected) Amazondata

# G is directed graph (for each undirected edge two directions)
with open('DBLPGraph.pickle', 'rb') as handle:
     G = pickle.load(handle)

# d_weights is dict with edge weights.
with open('DBLPWeightsAssortivity.pickle', 'rb') as handle:
    d_weights= pickle.load(handle)

#Performance(G, G, 500, 4, 4, 0.8, 4, d_weights, 'PerformanceDBLPQ4.txt', 'TimingDBLPQ4.txt')
#Performance(G, G, 500, 8, 4, 0.8, 4, d_weights, 'PerformanceDBLPQ8.txt', 'TimingDBLPQ8.txt')
Performance(G, G, 500, 12, 4, 0.8, 4, d_weights, 'PerformanceDBLPQ12.txt', 'TimingDBLPQ12.txt')
