from Functions import *

# The script for running tests on the (undirected) DBLP data (converted from the ACM network

with open('DBLP_Leman_Graph.pickle', 'rb') as handle:
     G = pickle.load(handle)

with open('DBLP_Leman_Weights_Assortivity.pickle', 'rb') as handle:
     d_weights_Assort = pickle.load(handle)

with open('DBLP_Leman_Weights_IndDegree.pickle', 'rb') as handle:
     d_weights_Ind = pickle.load(handle)

with open('DBLP_Leman_Weights_Neutral.pickle', 'rb') as handle:
     d_weights_Neutral = pickle.load(handle)

# Akoglu DBLP query (NIPS vs. PODS)
query = {'14870', '59562', '5280', '64966', '12266', '28459', '13841', '179607', '12853', '16388'}
tree_depth = 8
V = len(G.nodes())

with open('NIPSvsPODS_Forest_Assort.txt', 'w') as f:
    F = SteinerForest(G, query, tree_depth, d_weights_Assort, V)[1]
    f.write(repr(F))

with open('NIPSvsPODS_Tree_Assort.txt', 'w') as f:
    T = SteinerTree(G, query, tree_depth, d_weights_Assort, V)[1]
    f.write(repr(T))

with open('NIPSvsPODS_Forest_IndDegree.txt', 'w') as f:
    F = SteinerForest(G, query, tree_depth, d_weights_Ind, V)[1]
    f.write(repr(F))

with open('NIPSvsPODS_Tree_IndDegree.txt', 'w') as f:
    T = SteinerTree(G, query, tree_depth, d_weights_Ind, V)[1]
    f.write(repr(T))

with open('NIPSvsPODS_Forest_Neutral.txt', 'w') as f:
    F = SteinerForest(G, query, tree_depth, d_weights_Neutral, V)[1]
    f.write(repr(F))

with open('NIPSvsPODS_Tree_Neutral.txt', 'w') as f:
    T = SteinerTree(G, query, tree_depth, d_weights_Neutral, V)[1]
    f.write(repr(T))

