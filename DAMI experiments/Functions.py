from itertools import product
import collections
from math import log
from math import floor
import networkx as nx
import random
import copy
import pickle
from time import time

##########################################
##### Functions for Graph exploration ####
##########################################

def all_simple_paths_LookAhead(G, source, target, cutoff=None):
    # An improved version of the all_simple_paths algorithm in networkx
    if source not in G:
        raise nx.NetworkXError('source node %s not in graph'%source)
    if target not in G:
        raise nx.NetworkXError('target node %s not in graph'%target)
    if cutoff is None:
        cutoff = len(G)-1
    return _all_simple_paths_graph_LookAhead(G, source, target, cutoff=cutoff)

def _all_simple_paths_graph_LookAhead(G, source, target, cutoff=None):
    if cutoff < 1:
        return
    SP_target = nx.shortest_path_length(G, source=None, target=target)

    for x in (n for n in G.nodes() if n not in SP_target): # not all nodes can reach target -> they get a high value in SP_target
        SP_target[x] = cutoff+1
    
    visited = [source]
    stack = [iter(x for x in G[source] if 1 + SP_target[x] <= cutoff)] # Only adding children that are not violating the cutoff
    
    while stack:
        children = stack[-1]
        child = next(children, None)
        if child is None:
            stack.pop()
            visited.pop()
        elif len(visited) < cutoff:
            if child == target:
                yield visited + [target]
            elif child not in visited:
                visited.append(child)
                stack.append(iter(x for x in G[child] if len(visited) + SP_target[x] <= cutoff)) # Only add children that are not violating the cutoff
        else: #len(visited) == cutoff:
            if child == target or target in children:
                yield visited + [target]
            stack.pop()
            visited.pop()

def BFS(G, s, maxlevel): 
    level = {s: 0}
    #parent = {s: None}
    i = 1
    current_f = [s]
    while i <= maxlevel:
        next_f = []
        for u in current_f:
            for v in G.neighbors(u):
                if v not in level:
                    level[v] = i
                    next_f.append(v)
                    #parent[v] = u
        current_f = next_f   
        i += 1
    return level
           
def Common(G, query, maxlevel):
    Anc = {}
    l = []
    for q in query:
        level_dict = BFS(G, q, maxlevel)
        Anc[q] = set(level_dict.keys())
        l.append(Anc[q])
    return set.intersection(*l), set.union(*l)


def createShortestPaths(G, pruned_space, root, query, maxlevel, weight_dict):
    H = nx.DiGraph()
    pruning = G.subgraph(pruned_space)
    for q in (x for x in query if x != root):
        for path in all_simple_paths_LookAhead(pruning, source=root, target=q, cutoff=maxlevel):
            H.add_path(path)
    for e in H.edges():
        #---- USE THIS LINE IF WE WORK WITH G AND WEIGHT_DICT(G) ----
        #H[e[0]][e[1]]['weight']= -log(weight_dict[e],2)
        #---- USE THIS LINE IF WE WORK WITH GREV AND A WEIGH_DICT(G)---. 
        H[e[0]][e[1]]['weight']= -log(weight_dict[(int(e[1]),int(e[0]))],2)
        #---- USE THIS LINE IF WE HAVE A WEIGHTED INPUT GRAPH (graph has to be downward!). 
        #H[e[0]][e[1]]['weight']=G[e[0]][e[1]]['weight']
        
    return H

def checkChildren2(H, source, target, SP_dict, maxlevel, Steiner, level, query):

    SP_upd = {source: SP_dict[target]+1}
    if SP_upd[source] == SP_dict[source]:
        return True, SP_upd
    if SP_upd[source] + level[source] > maxlevel:
        return False,

    children = H.successors(source) # H flows from root to query nodes.
    while children:
        next_children = set()
        for c in (x for x in children if x != source):
            if c in Steiner: # This is sufficient because nodes in Steiner do not have incoming edges other than the one in Steiner (we removed them in the heuristics).
                SP_upd[c] = SP_upd[Steiner[c]]+1 # The root can never be a child, so Steiner[c] is always defined
                if SP_upd[c] + level[c] > maxlevel: # Checking if this would lead to a violated shortest path length constraint.
                    return False,
                if c == target:
                    # print('Cycle avoided!', flush=True)
                    return False, # This will induce a cycle, since target is in Steiner. 
                next_children.update(H.successors(c))
            else:
                parents = H.predecessors(c)
                kand = SP_upd.get(c, SP_dict[c])
                SP_upd[c] = min([SP_upd[x] for x in parents if x in SP_upd] + [SP_dict[x] for x in parents if not x in SP_upd]) + 1
                if c in query and SP_upd[c]>maxlevel: # c is a query node not in Steiner, so it has level = 0
                    return False,
                if SP_upd[c]>kand:
                    if c == target:
                        # print('Possible cycle avoided!', flush=True)
                        return False, # This MIGHT induce cycles (although the edge might be feasible), but we avoid this edge. This is OK since there will always be another feasible edge from source.
                    next_children.update(H.successors(c)) # Only consider children of nodes that were updated.

        children = next_children
                
    return True, SP_upd

####### THE ARBORESCENCES HEURISTICS #######

def SteinerDir1(H, root, query, maxlevel):
    # This Algo adds nodes & corresponding edges to the tree, by selecting the node with the highest sum of information CONTENTs of the (allowable) edges incident on the frontier nodes. 
    # weight_dict is an edge probability dict.
    # Assume the level of the root is <= maxlevel.
    # H should be the output of createShortestPaths

    # Steiner is a forest dict. 
    Steiner = {root: None}
    
    # Some necessary checks to ensure the tree we return doesn't exceed the maxlevel property.  
    SP_dict = nx.shortest_path_length(H, source=root) # A shortest path lengths dict. from the root node to all the other nodes. 
    
    frontier = set(query)
    try: frontier.remove(root)
    except KeyError: pass

    level_dict = dict((k, 0) for k in frontier) # A level dict for the Steiner tree.
        
    parent_front = {}
    # This is a dict with parents as keys and with values are sets of nodes in frontier.
    # Initialise parent_front.
    for i in frontier:
        for j in H.in_edges(i):
            if j[0] in parent_front:
                parent_front[j[0]].add(j[1])
            else: parent_front[j[0]] = {j[1]}

    removedEdges = []
    while frontier:
        # print(frontier)
        # This finds the best node (with the highest total weight to the frontier nodes.)
        max_gewicht = -100000
        for kandidaat_node in parent_front:
            goodEdges = []
            for x in parent_front[kandidaat_node]:
                check = checkChildren2(H, x, kandidaat_node, SP_dict, maxlevel, Steiner, level_dict, query)
                if check[0]: # Storing the allowable edges.
                    goodEdges.append(x) 
            kandidaat_gewicht = sum(H[kandidaat_node][x]['weight'] for x in goodEdges)
            if  (kandidaat_gewicht > max_gewicht) and goodEdges:
                goodEdgesPool = goodEdges
                max_gewicht = kandidaat_gewicht
                winner = kandidaat_node

        # From goodEdgesPool we one by one (according to IC) add edges and see if it is allowed (SP_dict)
        gewichtenLijst = [H[winner][x]['weight'] for x in goodEdgesPool]
        indices = [i[0] for i in sorted(enumerate(gewichtenLijst), key=lambda x:x[1], reverse=True)] # This tells you what indices to look for in goodEdgesPool.

        teller = 0
        max_teller = len(indices)
        while True:
            kand = goodEdgesPool[indices[teller]]
            check = checkChildren2(H, kand, winner, SP_dict, maxlevel, Steiner, level_dict, query)
            if check[0]:
                # Add the edge and perform the nesc. updates
                
                ## Updating the SP_dict
                SP_dict.update(check[1])
        
                ## Updating Steiner
                Steiner[kand] = winner

                ## Updating the level of the winner
                kand_level = level_dict[kand] + 1
                if winner in level_dict:
                    if kand_level > level_dict[winner]:
                        level_dict[winner] = kand_level
                        start = winner
                        while start in Steiner and start is not root:
                            parent = Steiner[start]
                            if level_dict[parent] >= level_dict[start]+1:
                                start = None
                            else:
                                level_dict[parent] = level_dict[start]+1
                                start = parent      
                else:
                    level_dict[winner] = kand_level

                ## Deleting kand from frontier. Removing all other edges incident on the node kand from H, and remove them from parent_front.
                x=[]
                frontier.remove(kand)
                for u in parent_front.values():
                    if kand in u:
                        u.remove(kand)
                parent_front = dict((k, v) for k, v in parent_front.items() if v)
                x.extend(H.in_edges(kand, data=True))
                x.remove((winner, kand, H.get_edge_data(winner, kand)))
                removedEdges.extend(x)
                H.remove_edges_from(x)
            teller += 1
            if teller == max_teller: break      
        # We add the winner to the frontier, and add the parent of the winner to parent_front.
        if winner not in Steiner:
            frontier.add(winner) 
            for j in H.in_edges(winner):
                if j[0] in parent_front:
                    parent_front[j[0]].add(j[1])
                else: parent_front[j[0]] = {j[1]}

    H.add_edges_from(removedEdges) # Restore the original graph H.
    
    return Steiner

def SteinerDir2(H, root, query, maxlevel, V):
    # This Algo adds nodes & corresponding edges to the tree, by selecting the node with the highest sum of information CONTENTs of the (allowable) edges incident on the frontier nodes. 
    # weight_dict is an edge probability dict.
    # Assume the level of the root is <= maxlevel.
    # H should be the output of createShortestPaths

    # Steiner is a forest dict. 
    Steiner = {root: None}
    q_len = len(query)
    
    # Some necessary checks to ensure the tree we return doesn't exceed the maxlevel property.  
    SP_dict = nx.shortest_path_length(H, source=root) # A shortest path lengths dict. from the root node to all the other nodes. 
    
    frontier = set(query)
    try: frontier.remove(root)
    except KeyError: pass

    level_dict = dict((k, 0) for k in frontier) # A level dict for the Steiner tree.
        
    parent_front = {}
    # This is a dict with parents as keys and with values are sets of nodes in frontier.
    # Initialise parent_front.
    for i in frontier:
        for j in H.in_edges(i):
            if j[0] in parent_front:
                parent_front[j[0]].add(j[1])
            else: parent_front[j[0]] = {j[1]}

    removedEdges = []
    while frontier:
        # print(frontier)
        # This finds the best node (with the highest total weight to the frontier nodes.)
        max_gewicht = -100000
        for kandidaat_node in parent_front:
            if (kandidaat_node in Steiner or kandidaat_node in Steiner.values() or kandidaat_node in query):
                n = len(set(Steiner.keys()).union(query))    
            else:
                n = len(set(Steiner.keys()).union(query))+1
            goodEdges = []
            for x in parent_front[kandidaat_node]:
                check = checkChildren2(H, x, kandidaat_node, SP_dict, maxlevel, Steiner, level_dict, query)
                if check[0]: # Storing the allowable edges.
                    goodEdges.append(x) 
            kandidaat_gewicht = sum(H[kandidaat_node][x]['weight'] for x in goodEdges)/((n-q_len+1)*log(V-q_len+1,2)+n*log(n+1,2))
            if  (kandidaat_gewicht > max_gewicht) and goodEdges:
                goodEdgesPool = goodEdges
                max_gewicht = kandidaat_gewicht
                winner = kandidaat_node

        # From goodEdgesPool we one by one (according to IC) add edges and see if it is allowed (SP_dict)
        gewichtenLijst = [H[winner][x]['weight'] for x in goodEdgesPool]
        indices = [i[0] for i in sorted(enumerate(gewichtenLijst), key=lambda x:x[1], reverse=True)] # This tells you what indices to look for in goodEdgesPool.

        teller = 0
        max_teller = len(indices)
        while True:
            kand = goodEdgesPool[indices[teller]]
            check = checkChildren2(H, kand, winner, SP_dict, maxlevel, Steiner, level_dict, query)
            if check[0]:
                # Add the edge and perform the nesc. updates
                
                ## Updating the SP_dict
                SP_dict.update(check[1])
        
                ## Updating Steiner
                Steiner[kand] = winner

                ## Updating the level of the winner
                kand_level = level_dict[kand] + 1
                if winner in level_dict:
                    if kand_level > level_dict[winner]:
                        level_dict[winner] = kand_level
                        start = winner
                        while start in Steiner and start is not root:
                            parent = Steiner[start]
                            if level_dict[parent] >= level_dict[start]+1:
                                start = None
                            else:
                                level_dict[parent] = level_dict[start]+1
                                start = parent      
                else:
                    level_dict[winner] = kand_level

                ## Deleting kand from frontier. Removing all other edges incident on the node kand from H, and remove them from parent_front.
                x=[]
                frontier.remove(kand)
                for u in parent_front.values():
                    if kand in u:
                        u.remove(kand)
                parent_front = dict((k, v) for k, v in parent_front.items() if v)
                x.extend(H.in_edges(kand, data=True))
                x.remove((winner, kand, H.get_edge_data(winner, kand)))
                removedEdges.extend(x)
                H.remove_edges_from(x)
            teller += 1
            if teller == max_teller: break      
        # We add the winner to the frontier, and add the parent of the winner to parent_front.
        if winner not in Steiner:
            frontier.add(winner) 
            for j in H.in_edges(winner):
                if j[0] in parent_front:
                    parent_front[j[0]].add(j[1])
                else: parent_front[j[0]] = {j[1]}

    H.add_edges_from(removedEdges) # Restore the original graph H.
    
    return Steiner

def SteinerDir3(H, root, query, maxlevel, V):
    # This Algo selects the 'best' edge, based on the highest gain in IR amongst all the possible edges that can be added. 
    # weight_dict is an edge probability dict.
    # H should be the output of createShortestPaths

    # Steiner is a forest dict.
    Steiner = {root: None}
    q_len = len(query)

    # Some necessary checks to ensure the tree we return doesn't exceed the maxlevel property.  
    SP_dict = nx.shortest_path_length(H, source=root) # A shortest path lengths dict. from the root node to all the other nodes. 
    
    frontier = set(query)
    try: frontier.remove(root)
    except KeyError: pass

    level_dict = dict((k, 0) for k in frontier) # A level dict for the Steiner tree.    

        
    parent_front = {}
    # This is a dict with parents as keys and with values are sets of nodes in frontier.
    # Initialise parent_front.
    for i in frontier:
        for j in H.in_edges(i):
            if j[0] in parent_front:
                parent_front[j[0]].add(j[1])
            else: parent_front[j[0]] = {j[1]}

    removedEdges = []
    while frontier:
        # This finds the best node (with the highest total weight to the frontier nodes.)
        max_gewicht = -100000
        #print(frontier)
        gewichtenLijst = []
        edgeLijst = []
        for kandidaat_node in parent_front:
            if (kandidaat_node in Steiner or kandidaat_node in Steiner.values() or kandidaat_node in query):
                n = len(set(Steiner.keys()).union(query))
            else:
                n = len(set(Steiner.keys()).union(query))+1
            for x in parent_front[kandidaat_node]:
                kandidaat_gewicht = H[kandidaat_node][x]['weight']/((n-q_len+1)*log(V-q_len+1,2)+n*log(n+1,2))
                gewichtenLijst.append(kandidaat_gewicht)
                edgeLijst.append((x, kandidaat_node))
        
        # Finding the highest IR edge that CAN be added.
        indices = [i[0] for i in sorted(enumerate(gewichtenLijst), key=lambda x:x[1], reverse=True)] # This tells you what indices to look for in edgeLijst.
        teller = 0
        while True:
            source = edgeLijst[indices[teller]][0]
            target = edgeLijst[indices[teller]][1]
            check = checkChildren2(H, source, target, SP_dict, maxlevel, Steiner, level_dict, query)
            if len(check)==2:
                winner2 = source
                winner = target
                SP_kand = check[1]
                break
            teller += 1
        
        # Updating SP_dict:
        SP_dict.update(SP_kand)
                        
        # Add the result to Steiner (which is a forrest, but finalizes as a tree).
        Steiner.update({winner2: winner})

        # Updating the level dict.
        kand_level = level_dict[winner2]+1 
        if winner in level_dict:
            if kand_level > level_dict[winner]:
                level_dict[winner] = kand_level
                start = winner
                while start in Steiner and start is not root:
                    parent = Steiner[start]
                    if level_dict[parent] >= level_dict[start]+1:
                        start = None
                    else:
                        level_dict[parent] = level_dict[start]+1
                        start = parent      
        else:
            level_dict[winner] = kand_level
  
        # We delete the corresponding nodes from frontier.  We remove all edges incident on the nodes in parent_front too (in order to remain a tree).
        frontier.remove(winner2)
        for u in parent_front.values():
            if winner2 in u:
                u.remove(winner2)

        x = H.in_edges(winner2, data=True)
        x.remove((winner, winner2, H.get_edge_data(winner, winner2)))
                 
        H.remove_edges_from(x)
        removedEdges.extend(x)
                
        # We remove empty values from the dictionairy.
        parent_front = dict((k, v) for k, v in parent_front.items() if v)
        
        # We add the winner to the frontier, and add the parent of the winner to parent_front.
        if winner not in Steiner:
            frontier.add(winner) 
            for j in H.in_edges(winner):
                if j[0] in parent_front:
                    parent_front[j[0]].add(j[1])
                else: parent_front[j[0]] = {j[1]}

    H.add_edges_from(removedEdges)
    return Steiner


def SteinerDir4(H, root, query, maxlevel, V):
    # This adds the 'best' edge from the 'best' parent node. 'best' parent node is based on the possible IR gain of adding all it's edges to frontier nodes. 

    # weight_dict is an edge probability dict.
    # Greedy algorithm for finding a (downward) Steiner Arborescence with maximally informative edges. Assume the level of the root is <= k.
    # Apply Grev for a upward Steiner Arborescence.
    # We start by creating a subgraph with ALL the paths from r->q_i. The result is a DAG. If k is small, this is feasible.
    Steiner = {root: None}
    q_len = len(query)

    # Some necessary checks to ensure the tree we return doesn't exceed the maxlevel property.  
    SP_dict = nx.shortest_path_length(H, source=root) # A shortest path lengths dict. from the root node to all the other nodes. 
     
    frontier = set(query)
    try: frontier.remove(root)
    except KeyError: pass

    level_dict = dict((k, 0) for k in frontier) # A level dict for the Steiner tree.
        
    parent_front = {}
    # This is a dict with parents as keys and with values are sets of nodes in frontier.
    # Initialise parent_front.
    for i in frontier:
        for j in H.in_edges(i):
            if j[0] in parent_front:
                parent_front[j[0]].add(j[1])
            else: parent_front[j[0]] = {j[1]}

    removedEdges = []
    while frontier:
        # This finds the best 'parent' node (with the highest total edge weights to the frontier nodes.)
        max_gewicht = -100000
        for kandidaat_node in parent_front:
            if (kandidaat_node in Steiner or kandidaat_node in Steiner.values() or kandidaat_node in query): #and (kandidaat_node is not root):
                n = len(set(Steiner.keys()).union(query))  
            else:
                n = len(set(Steiner.keys()).union(query))+1
            # Only taking in account edges that don't violate the maxlevel property!
            goodEdges = [x for x in parent_front[kandidaat_node] if checkChildren2(H, x, kandidaat_node, SP_dict, maxlevel, Steiner, level_dict, query)[0]]
            kandidaat_gewicht = sum(H[kandidaat_node][x]['weight'] for x in goodEdges)/((n-q_len+1)*log(V-q_len+1,2)+n*log(n+1,2)) 
            if (kandidaat_gewicht > max_gewicht) and goodEdges:
                goodEdgesFinal = goodEdges
                max_gewicht = kandidaat_gewicht
                winner = kandidaat_node

        max_gewicht = -100000        
        for x in goodEdgesFinal:
            kandidaat_gewicht = H[winner][x]['weight']
            if kandidaat_gewicht > max_gewicht:
                max_gewicht = kandidaat_gewicht
                winner2 = x

        # Update SP_dict
        SP_dict.update(checkChildren2(H, winner2, winner, SP_dict, maxlevel, Steiner, level_dict, query)[1])        
              
        # Add the result to Steiner (which is a forrest, but finalizes as a tree).
        Steiner.update({winner2: winner})

        # Updating the level of the winner (taking in account that the winner may already be in the Steiner tree)
        kand_level = level_dict[winner2] + 1
        
        if winner in level_dict:
            if kand_level > level_dict[winner]:
                level_dict[winner] = kand_level
                start = winner
                while start in Steiner and start is not root:
                    parent = Steiner[start]
                    if level_dict[parent] >= level_dict[start]+1:
                        start = None
                    else:
                        level_dict[parent] = level_dict[start]+1
                        start = parent      
        else:
            level_dict[winner] = kand_level
       
        # We delete the corresponding nodes from frontier.  We remove all edges incident on the nodes in parent_front too (in order to remain a tree).
        frontier.remove(winner2)
        for u in parent_front.values():
            if winner2 in u:
                u.remove(winner2)

        x = H.in_edges(winner2, data=True)
        x.remove((winner, winner2, H.get_edge_data(winner, winner2)))
        H.remove_edges_from(x)
        removedEdges.extend(x)
                
        # We remove empty values from the dictionairy.
        parent_front = dict((k, v) for k, v in parent_front.items() if v)
        
        # We add the winner to the frontier, and add the parent of the winner to parent_front.
        if winner not in Steiner:
            frontier.add(winner) 
            for j in H.in_edges(winner):
                if j[0] in parent_front:
                    parent_front[j[0]].add(j[1])
                else: parent_front[j[0]] = {j[1]}

    H.add_edges_from(removedEdges)
    return Steiner

#### (Undirected) Trees Heuristic ####

def SteinerTree(G, query, maxlevel, weight_dict, V):
    # Assume G is the bidirectional variant of an undirected graph.
    # Assume weight_dict has equal weights for the edges (u,v) and (v,u)
    # BestTree is returned as a dict. (arborescence), however it should be interpreted as an undirected tree.
    
    SP_QueryToQuery = {x: max(nx.shortest_path_length(G, x, y) for y in query) for x in query}
    reduced_level = floor(maxlevel/2)
    GoodQuery = {x for x in SP_QueryToQuery if SP_QueryToQuery[x] <= reduced_level} # Only keeping query nodes as root candidates that are reachable from all the other queries.
    if not GoodQuery:
        # raise ValueError("Could not find such a connecting tree: MAXLEVEL too low")
        return False, {}
    
    _, pruned_space = Common(G, query, reduced_level)
    maxIR = -10000
    for q in GoodQuery:
        H = createShortestPaths(G, pruned_space, q, query, reduced_level, weight_dict)
        Tree = SteinerDir2(H, q, query, reduced_level, V)
        kandIR = ICup(Tree, weight_dict)/DLTree(Tree, len(query), V)
        if kandIR > maxIR:
            maxIR = kandIR
            BestTree = Tree
    return True, BestTree


#### (Directed) Branching Heuristic ####

def SteinerBranching(G, Grev, query, maxlevel, weight_dict, V):
    _, pruned_space = Common(G, query, maxlevel)
    H = Grev.subgraph(pruned_space)
    for e in H.edges():
        H[e[0]][e[1]]['weight']= -log(weight_dict[(int(e[1]),int(e[0]))],2)     
    for n in H.nodes():
        H.add_edge('ROOT',n)
        H['ROOT'][n]['weight'] = 0
    Branching = SteinerDir2(H, 'ROOT', query, maxlevel+1, V)
    # Removing the edges to the root, and counting the number of arbo's in the branching.
    numberofTrees = 0
    for n in Branching:
        if Branching[n] == 'ROOT':
            Branching[n] = None
            numberofTrees += 1
    Branching.pop('ROOT')
    return numberofTrees, Branching

#### (Undirected) Forest Heuristic ####

def SteinerForest(G, query, maxlevel, weight_dict, V):
    # Assume G is the bidirectional variant of an undirected graph.
    # Assume weight_dict has equal weights for the edges (u,v) and (v,u)
    # BestTree is returned as a dict. (branching), however it should be interpreted as an undirected forest.
    reduced_level = floor(maxlevel/2)
    H = nx.DiGraph()
    _, pruned_space = Common(G, query, reduced_level)
    subG = G.subgraph(pruned_space)
    for n in query:
        subG.add_edge('ROOT',n)       
    for q in (x for x in query if x != 'ROOT'):
        for path in all_simple_paths_LookAhead(subG, source='ROOT', target=q, cutoff=reduced_level+1):
            H.add_path(path)
    for e in (z for z in H.edges() if z not in (('ROOT',q) for q in query)):
        H[e[0]][e[1]]['weight']= -log(weight_dict[(int(e[1]),int(e[0]))],2)
    for n in query:
        H['ROOT'][n]['weight'] = 0

    Forest = SteinerDir2(H, 'ROOT', query, reduced_level+1, V)
    # Removing the edges to the root, and counting the number of trees in the forest.
    numberofTrees = 0
    for n in Forest:
        if Forest[n] == 'ROOT':
            Forest[n] = None
            numberofTrees += 1
    Forest.pop('ROOT')
    return numberofTrees, Forest

#### Trees IC and DL ####

def ICup(Tree, weights_dict):
    # Information Content of an 'upward' tree:
    ic = 0
    for i in Tree:
        if Tree[i] != None:
            ic += -log(weights_dict[(int(i), int(Tree[i]))],2)   
    return ic

def DLTree(Tree, q, V):
    # q = query length.
    # V = number of nodes in the graph. 
    n = len(Tree)
    return (n-q+1)*log(V-q+1,2)+n*log(n+1,2)

###################################
#### Functions for experiments ####
###################################

#### Functions for average/max over all arbos, given 1 root ####

def MergeTree(T1, T2):
    merge = dict(T1)
    for u in T2:
        if u in T1 and T2[u] != T1[u]:
            return {}
        elif u not in T1:
            merge[u]=T2[u]
    return merge

def random_product(*args, repeat=1):
    "Random selection from itertools.product(*args, **kwds)"
    pools = [tuple(pool) for pool in args] * repeat
    return tuple(random.choice(pool) for pool in pools)

def AllTreesOneRoot(G, query, root, maxlevel, maxtrees):
    # Grev = nx.reverse(G)
    # Use G downward trees, use Grev for upwards trees.
    # With a BREAK level for the number of max trees. 
    trees = []
    if root in query:
        query.remove(root)

    minIterations=5
    teller=0
    while teller<minIterations:
        paths = []
        limitPerQuery = floor(maxtrees**(1.0/len(query)))    
        for q in query:
            PadenLijst = []
            pathsToQuery = list(all_simple_paths_LookAhead(G, source=root, target=q, cutoff=maxlevel))
            random.shuffle(pathsToQuery)
            for k in pathsToQuery:
                d = {k[i]:k[i-1] for i in range(1,len(k))}
                PadenLijst.append(d)
                if len(PadenLijst)==limitPerQuery:
                    break
            paths.append(PadenLijst)
        
        kand = product(*paths)
        for z in kand:
            y = [dict(x) for x in z]
            Tree = dict()
            valid = True
            for d in y:
                for k, v in d.items():
                    # Tree.setdefault(k,set()).add(v)
                    if Tree.get(k, v) != v:
                        valid = False # Can't merge the two paths into a valid tree
                        break
                    Tree[k] = v
                else:
                    continue
                break
            if valid:
                Tree.update({root: None})
                trees.append(Tree)
        teller+=1
    return trees

def MergeTree(tree1, tree2):
    feasible = True
    for k, v in tree2.items():
        if tree1.get(k, v) != v:
            feasible = False
            break
    if feasible:
        tree1.update(tree2)
    return feasible

def maxIRup(Trees, weights_dict, q, V):  
    maxIR = 0
    for t in Trees:
        kand = round(ICup(t, weights_dict)/DLTree(t, q, V), 5)
        if kand > maxIR:
            maxIR = kand
            maxTree = t
    return maxTree, maxIR

def avgIRup(Trees, weights_dict, q, V):  
    avgIR = 0
    teller=0
    for t in Trees:
        avgIR += round(ICup(t, weights_dict)/DLTree(t, q, V), 5)
        teller += 1
    return avgIR/teller

#### Function for generating a random query set #### 

def GenerateQuerySet(G, node_initial, k, beamwidth, s):
    initNeighbors = G.neighbors(node_initial)
    cands = set(random.sample(initNeighbors, min(len(initNeighbors),beamwidth)))
    neighborsPicked = set()
    neighborsPicked.add(node_initial)
    query = set()
    query.add(node_initial)
    while len(query) < k and len(cands)>0:
        queryaddtest = set()
        candsNow = cands.copy()
        for i in candsNow:
            if random.uniform(0,1) < s:
                queryaddtest.add(i)
                cands.remove(i)
            if i not in neighborsPicked:
                neighborsPicked.add(i)
                iNeighbors = G.neighbors(i)
                cands.update(set(random.sample(iNeighbors, min(len(iNeighbors),beamwidth))))
        query.update(set(random.sample(queryaddtest,min(k-len(query),len(queryaddtest)))))
    return query

#### Function for evaluating the relative performance of the SteinerDir heuristics #### 

def Performance(G, Grev, numberoftrees, querysize, beamwidth, s, tree_depth, d_weights, performanceFile, timingFile):
    with open(performanceFile, 'w') as Performance, open(timingFile, 'w') as Timing:
        MAX_TREES = 2000000
        teller = 0
        V = len(G.nodes())
        while teller < numberoftrees:
            init_node = random.choice(G.nodes())
            query = GenerateQuerySet(G, init_node, querysize, beamwidth, s)
            commonRoots, pruned_space = Common(G, query, tree_depth)     
            if len(query) == querysize and len(commonRoots)>0:
                random_root = random.choice(list(commonRoots))
                print(random_root,flush=True)
                print(query, flush=True)
                
                # Timing
                t0 = time()
                H = createShortestPaths(Grev, pruned_space, random_root, query, tree_depth, d_weights)
                t1 = time()
                time_SP = round(t1-t0,5)
                
                t0 = time()
                S1 = SteinerDir1(H, random_root, query, tree_depth)
                t1 = time()
                time_s1 = round(t1-t0,5)
                
                t0 = time()
                S2 = SteinerDir2(H, random_root, query, tree_depth, V)
                t1 = time()
                time_s2 = round(t1-t0,5)
                
                t0 = time()
                S3 = SteinerDir3(H, random_root, query, tree_depth, V)
                t1 = time()
                time_s3 = round(t1-t0,5)
                
                t0 = time()
                S4 = SteinerDir4(H, random_root, query, tree_depth, V)
                t1 = time()
                time_s4 = round(t1-t0,5)
                
                Trees = AllTreesOneRoot(H, query, random_root, tree_depth, MAX_TREES)
                
                if len(Trees)>200:
                    # Write Timing
                    Timing.write(repr(time_SP)+'\t'+repr(time_s1)+'\t'+repr(time_s2)+'\t'+repr(time_s3)+'\t'+repr(time_s4)+'\t'+repr(len(Trees))+'\n')

                    # Write Performance
                    ir_s1 = round(ICup(S1, d_weights)/DLTree(S1, querysize, V),5)
                    ir_s2 = round(ICup(S2, d_weights)/DLTree(S2, querysize, V),5)
                    ir_s3 = round(ICup(S3, d_weights)/DLTree(S3, querysize, V),5)
                    ir_s4 = round(ICup(S4, d_weights)/DLTree(S4, querysize, V),5)
                    maxir = maxIRup(Trees, d_weights, querysize, V)[1]

                    ratio_s1 = ir_s1/maxir
                    ratio_s2 = ir_s2/maxir
                    ratio_s3 = ir_s3/maxir
                    ratio_s4 = ir_s4/maxir
                    ratio_avg = avgIRup(Trees, d_weights, querysize, V)/maxir

                    Performance.write(repr(ratio_s1)+'\t'+repr(ratio_s2)+'\t'+repr(ratio_s3)+'\t'+repr(ratio_s4)+'\t'+repr(ratio_avg)+'\n')
                    teller +=1
                    print(teller,flush=True)

#### Function for evaluating the Speed of the SteinerDir, SteinerTree, SteinerBranching, SteinerForest heuristics ####
   
def Speed(G, Grev, numberoftrees, querysize, beamwidth, s, tree_depth, d_weights, timingFile):
    with open(timingFile, 'a') as Timing:
        teller = 0
        V = len(G.nodes())
        while teller < numberoftrees:
            init_node = random.choice(G.nodes())
            query = GenerateQuerySet(G, init_node, querysize, beamwidth, s)
            commonRoots, pruned_space = Common(G, query, tree_depth)     
            if len(query) == querysize and len(commonRoots)>0:
                random_root = random.choice(list(commonRoots))
                print(random_root,flush=True)
                print(query, flush=True)
                
                # Timing
                #t0 = time()
                #H = createShortestPaths(Grev, pruned_space, random_root, query, tree_depth, d_weights)
                #SteinerDir2(H, random_root, query, tree_depth, V)
                #t1 = time()
                time_Arbo = 0
                
                t0 = time()
                check = SteinerTree(G, query, tree_depth, d_weights, V)[0]
                t1 = time()
                time_Tree = round(t1-t0,5)
				
                
                t0 = time()
                nB, _ = SteinerBranching(G, G, query, tree_depth, d_weights, V)
                t1 = time()
                time_Branching = round(t1-t0,5)
                
                t0 = time()
                nT, _ = SteinerForest(G, query, tree_depth, d_weights, V)
                t1 = time()
                time_Forest = round(t1-t0,5)
                
                
                # Write Timing
                if check:
                    Timing.write(repr(time_Arbo)+'\t'+repr(time_Tree)+'\t'+repr(time_Branching)+'\t'+repr(time_Forest)+'\t'+repr(nB)+'\t'+repr(nT)+'\n') 
                    teller +=1
                    print(teller,flush=True)
