from itertools import product
from math import log
import networkx as nx
import random
import copy
from time import time

MAX_TREES = 200000

def BFS(G, s, maxlevel): 
    level = {s: 0}
    #parent = {s: None}
    i = 1
    current_f = [s]
    while i <= maxlevel:
        next_f = []
        for u in current_f:
            for v in G.neighbors(u):
                if v not in level:
                    level[v] = i
                    next_f.append(v)
                    #parent[v] = u
        current_f = next_f   
        i += 1
    return level

def Common(G, query, maxlevel):
    Anc = {}
    l = []
    for q in query:
        Anc[q] = set(BFS(G, q, maxlevel).keys())
        l.append(Anc[q])
    return set.intersection(*l), set.union(*l)


def MergeTree(T1, T2):
    merge = dict(T1)
    for u in T2:
        if u in T1 and T2[u] != T1[u]:
            return {}
        elif u not in T1:
            merge[u]=T2[u]
    return merge

def AllTreesOneRoot(G, query, root, maxlevel, maxtrees):
    # Grev = nx.reverse(G)
    # Use G downward trees, use Grev for upwards trees.
    # With a BREAK level for the number of max trees. 
    trees = []
    paths = []
    if root in query:
        PadenLijst = []
        query.remove(root)
        PadenLijst.append({root: None})
        paths.append(PadenLijst)
        
    for q in query:
        PadenLijst = []
        for k in list(nx.all_simple_paths(G, source=root, target=q, cutoff=maxlevel)):
            d = {k[i]:k[i-1] for i in range(1,len(k))}
            d[k[0]] = None 
            PadenLijst.append(d)

        paths.append(PadenLijst)

    # Estimating the max number of trees.
    '''
    teller = 0
    maxnumber = 1
    while teller<len(query):
        maxnumber *= len(paths[teller])
        teller += 1
    # print(maxnumber)
    if maxnumber >= maxtrees:
        # print('MAX_TREES reached')
        return []
    '''
    kand = product(*paths) 
    for z in kand:
        y = [dict(x) for x in z]
        teller = len(y)
        while teller>1:
            if not MergeTree(y[0],y[-1]): break
            else:
                y[0] = dict(MergeTree(y[0],y[-1]))
                del y[-1]
                teller -= 1
        if teller==1:
            trees.append(y[0])
        if len(trees)>=maxtrees: break
    return trees

def AllTrees(G, Grev, query, maxlevel):
    # Grev = nx.reverse(G)
    # Apply (G, Grev, ...) for downward trees, apply (Grev, G, ...) for upward trees. 
    roots, nodes = Common(Grev, query, maxlevel)
    trees = []
    Gsub = G.subgraph(nodes)
    for r in roots:
        paths = []
        for q in query:
            PadenLijst = []
            for k in list(nx.all_simple_paths(Gsub, source=r, target=q, cutoff=maxlevel)):
                d = {k[i]:k[i-1] for i in range(1,len(k))}
                d[k[0]] = None 
                PadenLijst.append(d)
            if not PadenLijst:
                d = {q: None}
                PadenLijst.append(d)
                
            paths.append(PadenLijst)

        kand = [list(x) for x in product(*paths)]
        for z in kand:
            y = [dict(x) for x in z]
            teller = len(y)
            while teller>1:
                if not MergeTree(y[0],y[-1]): break
                else:
                    y[0] = dict(MergeTree(y[0],y[-1]))
                    del y[-1]
                    teller -= 1
            if teller==1:
                trees.append(y[0])
    return trees

def ICdown(Tree, weights_dict):
    # Information Content of an 'downward' tree:
    ic = 0
    for i in Tree:
        if Tree[i] != None:
            ic += -log(weights_dict[(Tree[i], i)],2)   
    return ic

def ICup(Tree, weights_dict):
    # Information Content of an 'upward' tree:
    ic = 0
    for i in Tree:
        if Tree[i] != None:
            ic += -log(weights_dict[(i, Tree[i])],2)   
    return ic

def ICupPrint(Tree, weights_dict):
    # Information Content of an out-arborescence:
    for i in Tree:
        if Tree[i] != None:
            print(-log(weights_dict[i,Tree[i]],2))
    return None

def DLTree(Tree, q, V):
    # q = query length.
    # V = number of nodes in the graph. 
    n = len(Tree)
    return (n-q+1)*log(V-q+1,2)+n*log(n+1,2)

def maxIRup(Trees, weights_dict, q, V):  
    maxIR = 0
    for t in Trees:
        kand = round(ICup(t, weights_dict)/DLTree(t, q, V), 5)
        if kand > maxIR:
            maxIR = kand
            maxTree = t
    return maxTree, maxIR

def avgIRup(Trees, weights_dict, q, V):  
    avgIR = 0
    teller=0
    for t in Trees:
        avgIR += round(ICup(t, weights_dict)/DLTree(t, q, V), 5)
        teller += 1
    return avgIR/teller

'''
def checkChildren(H, source, target, SP_dict, maxlevel, frontier, Steiner, level):
    # Checking if adding the (source,target)-edge violates the maxlevel property of a frontier node. 
    # If no errors, we return an updated shortest path dict.
    # Assume H flows from top to bottom.

    frontier2 = frontier.copy()
    SP_kand = {source: SP_dict[target]+1}
    if SP_kand[source] == SP_dict[source]:
        return True, SP_kand
    if SP_kand[source] + level[source] > maxlevel:
        return False,

    frontier2.remove(source)
    children = H.successors(source)
    while frontier2 and children:
        next_children = []
        for child in children:
            if child not in Steiner:
                parents = H.predecessors(child)
                kand_min = min([SP_kand[x] for x in parents if x in SP_kand] + [SP_dict[x] for x in parents if x not in SP_kand]) + 1
                if kand_min > SP_dict[child]:
                    SP_kand[child] = kand_min
                    if child in frontier2:
                        if SP_kand[child] + level[child] > maxlevel:
                            return False,
                        else: frontier2.remove(child)
                    next_children.extend(H.successors(child))
            else:
                SP_kand[child] = SP_kand[Steiner[child]]+1
                next_children.extend(H.successors(child))
        children = next_children
    return True, SP_kand
'''

def checkChildren2(H, source, target, SP_dict, maxlevel, Steiner, level, query):

    SP_upd = {source: SP_dict[target]+1}
    if SP_upd[source] == SP_dict[source]:
        return True, SP_upd
    if SP_upd[source] + level[source] > maxlevel:
        return False,

    children = H.successors(source)
    while children:
        next_children = set()
        for c in (x for x in children if x != source):
            if c in Steiner: # This is sufficient because nodes in Steiner do not have outgoing edges other than the one in Steiner (we removed them in the heuristics).
                SP_upd[c] = SP_upd[Steiner[c]]+1 # The root can never be a child.
                if SP_upd[c] + level[c] > maxlevel: # Checking if this would lead to a violated shortest path length constraint.
                    return False,
                if c == target:
                    print('Possible cycle avoided!', flush=True)
                    return False, # This MIGHT induce cycles (although the edge might be feasible), but we avoid this edge. This is OK since there will always be another feasible edge from source.
                next_children.update(H.successors(c))
            else:
                parents = H.predecessors(c)
                kand = SP_upd.get(c, SP_dict[c])
                SP_upd[c] = min([SP_upd[x] for x in parents if x in SP_upd] + [SP_dict[x] for x in parents if not x in SP_upd]) + 1
                if c in query and SP_upd[c]>maxlevel: # c is a query node not in Steiner, so it has level = 0
                    return False,
                if SP_upd[c]>kand:
                    if c == target:
                        print('Possible cycle avoided!', flush=True)
                        return False, # This MIGHT induce cycles (although the edge might be feasible), but we avoid this edge. This is OK since there will always be another feasible edge from source.
                    next_children.update(H.successors(c)) # Only consider children of nodes that were updated.

        children = next_children
                
    return True, SP_upd

def createShortestPaths(G, pruned_space, root, query, maxlevel, weight_dict):
    H = nx.DiGraph()
    pruning = G.subgraph(pruned_space)
    for q in (x for x in query if x != root):
        for path in nx.all_simple_paths(pruning, source=root, target=q, cutoff=maxlevel):
            H.add_path(path)
    for e in H.edges():
        #---- USE THIS LINE IF WE WORK WITH G AND WEIGHT_DICT(G) ----
        #H[e[0]][e[1]]['weight']= -log(weight_dict[e],2)
        #---- USE THIS LINE IF WE WORK WITH GREV AND A WEIGH_DICT(G)---. 
        H[e[0]][e[1]]['weight']= -log(weight_dict[(e[1],e[0])],2)
        #---- USE THIS LINE IF WE HAVE A WEIGHTED INPUT GRAPH (graph has to be downward!). 
        #H[e[0]][e[1]]['weight']=G[e[0]][e[1]]['weight']
        
    return H


def SteinerDir1(H, root, query, maxlevel):
    # This Algo adds nodes & corresponding edges to the tree, by selecting the node with the highest sum of information CONTENTs of the (allowable) edges incident on the frontier nodes. 
    # weight_dict is an edge probability dict.
    # Assume the level of the root is <= maxlevel.
    # H should be the output of createShortestPaths

    # Steiner is a forest dict. 
    Steiner = {root: None}
    
    # Some necessary checks to ensure the tree we return doesn't exceed the maxlevel property.  
    SP_dict = nx.shortest_path_length(H, source=root) # A shortest path lengths dict. from the root node to all the other nodes. 
    
    frontier = set(query)
    try: frontier.remove(root)
    except KeyError: pass

    level_dict = dict((k, 0) for k in frontier) # A level dict for the Steiner tree.
        
    parent_front = {}
    # This is a dict with parents as keys and with values are sets of nodes in frontier.
    # Initialise parent_front.
    for i in frontier:
        for j in H.in_edges(i):
            if j[0] in parent_front:
                parent_front[j[0]].add(j[1])
            else: parent_front[j[0]] = {j[1]}

    removedEdges = []
    while frontier:
        # print(frontier)
        # This finds the best node (with the highest total weight to the frontier nodes.)
        max_gewicht = -100000
        SP_update_dict = {}
        for kandidaat_node in parent_front:
            goodEdges = []
            for x in parent_front[kandidaat_node]:
                check = checkChildren2(H, x, kandidaat_node, SP_dict, maxlevel, Steiner, level_dict, query)
                if len(check) == 2: # Storing the allowable edges.
                    SP_update_dict[(x, kandidaat_node)] = check[1]
                    goodEdges.append(x)
      
            kandidaat_gewicht = sum(H[kandidaat_node][x]['weight'] for x in goodEdges)
            if  kandidaat_gewicht > max_gewicht:
                goodEdgesPool = goodEdges
                max_gewicht = kandidaat_gewicht
                winner = kandidaat_node

        # From goodEdgesPool we one by one (according to IC) add edges and see if it is allowed (SP_dict)
        gewichtenLijst = [H[winner][x]['weight'] for x in goodEdgesPool]
        indices = [i[0] for i in sorted(enumerate(gewichtenLijst), key=lambda x:x[1], reverse=True)] # This tells you what indices to look for in goodEdgesPool.

        goodEdgesFinal = []
        teller = 0
        max_teller = len(indices)
        while True:
            kand = goodEdgesPool[indices[teller]]
            if checkChildren2(H, kand, winner, SP_dict, maxlevel, Steiner, level_dict, query)[0]:
                SP_dict.update(SP_update_dict[(kand, winner)])
                goodEdgesFinal.append(kand) 
                teller += 1
                if teller == max_teller: break
            else: break

                
        # Add the result to Steiner (which is a forrest, but finalizes as a tree).
        Steiner.update({k: winner for k in goodEdgesFinal})

        # Updating the level of the winner (taking in account that the winner may already be in the Steiner tree)
        kand_level = max(level_dict[k] for k in goodEdgesFinal) + 1
        if winner in level_dict:
            if kand_level > level_dict[winner]:
                level_dict[winner] = kand_level
                start = winner
                while start in Steiner and start is not root:
                    parent = Steiner[start]
                    if level_dict[parent] >= level_dict[start]+1:
                        start = None
                    else:
                        level_dict[parent] = level_dict[start]+1
                        start = parent      
        else:
            level_dict[winner] = kand_level    
    
        # We delete the corresponding nodes from frontier.  We remove all edges incident on the nodes in parent_front too (in order to remain a tree).
        x=[]
        for k in goodEdgesFinal:
            frontier.remove(k)
            for u in parent_front.values():
                if k in u:
                    u.remove(k)
            x.extend(H.in_edges(k, data=True)) # This gets the edge weights
            x.remove((winner, k, H.get_edge_data(winner, k)))
            
        removedEdges.extend(x)
        H.remove_edges_from(x)

        # We remove empty values from the dictionairy.
        parent_front = dict((k, v) for k, v in parent_front.items() if v)
        
        # We add the winner to the frontier, and add the parent of the winner to parent_front.
        if winner not in Steiner:
            frontier.add(winner) 
            for j in H.in_edges(winner):
                if j[0] in parent_front:
                    parent_front[j[0]].add(j[1])
                else: parent_front[j[0]] = {j[1]}

    H.add_edges_from(removedEdges) # Restore the original graph H.
    
    return Steiner

def SteinerDir2(H, root, query, maxlevel, V):
    # This Algo adds nodes & corresponding edges to the tree, by selecting the node with the highest total information RATIO of the (allowable) edges incident on the frontier nodes. 
    # weight_dict is an edge probability dict.
    # Assume the level of the root is <= maxlevel.
    # H should be the output of createShortestPaths

    # Steiner is a forest dict. 
    Steiner = {root: None}
    q_len = len(query)

    # Some necessary checks to ensure the tree we return doesn't exceed the maxlevel property.  
    SP_dict = nx.shortest_path_length(H, source=root) # A shortest path lengths dict. from the root node to all the other nodes. 
    
    frontier = set(query)
    try: frontier.remove(root)
    except KeyError: pass

    level_dict = dict((k, 0) for k in frontier) # A level dict for the Steiner tree.    
        
    parent_front = {}
    # This is a dict with parents as keys and with values are sets of nodes in frontier.
    # Initialise parent_front.
    for i in frontier:
        for j in H.in_edges(i):
            if j[0] in parent_front:
                parent_front[j[0]].add(j[1])
            else: parent_front[j[0]] = {j[1]}

    removedEdges = []
    while frontier:
        # This finds the best node (with the highest total weight to the frontier nodes.)
        max_gewicht = -100000
        SP_update_dict = {}
        for kandidaat_node in parent_front:
            if (kandidaat_node in Steiner or kandidaat_node in Steiner.values() or kandidaat_node in query):
                n = len(set(Steiner.keys()).union(query))    
            else:
                n = len(set(Steiner.keys()).union(query))+1
                
            goodEdges = []
            for x in parent_front[kandidaat_node]:
                check = checkChildren2(H, x, kandidaat_node, SP_dict, maxlevel, Steiner, level_dict, query)
                if len(check) == 2: # Storing the allowable edges.
                    SP_update_dict[(x, kandidaat_node)] = check[1]
                    goodEdges.append(x)
      
            kandidaat_gewicht = sum(H[kandidaat_node][x]['weight'] for x in goodEdges)/((n-q_len+1)*log(V-q_len+1,2)+n*log(n+1,2))
            if  kandidaat_gewicht > max_gewicht:
                goodEdgesPool = goodEdges
                max_gewicht = kandidaat_gewicht
                winner = kandidaat_node
                
        
        # From goodEdgesPool we one by one (according to IC) add edges and see if it is allowed (SP_dict)
        gewichtenLijst = [H[winner][x]['weight'] for x in goodEdgesPool]
        indices = [i[0] for i in sorted(enumerate(gewichtenLijst), key=lambda x:x[1], reverse=True)] # This tells you what indices to look for in goodEdgesPool.

        goodEdgesFinal = []
        teller = 0
        max_teller = len(indices)
        while True:
            kand = goodEdgesPool[indices[teller]]
            if checkChildren2(H, kand, winner, SP_dict, maxlevel, Steiner, level_dict, query)[0]:
                SP_dict.update(SP_update_dict[(kand, winner)])
                goodEdgesFinal.append(kand) 
                teller += 1
                if teller == max_teller: break
            else: break
             
                    
        # Add the result to Steiner (which is a forest, but finalizes as a tree).
        Steiner.update({k: winner for k in goodEdgesFinal})

        # Updating the level of the winner (taking in account that the winner may already be in the Steiner tree)
        kand_level = max(level_dict[k] for k in goodEdgesFinal) + 1
        if winner in level_dict:
            if kand_level > level_dict[winner]:
                level_dict[winner] = kand_level
                start = winner
                while start in Steiner and start is not root:
                    parent = Steiner[start]
                    if level_dict[parent] >= level_dict[start]+1:
                        start = None
                    else:
                        level_dict[parent] = level_dict[start]+1
                        start = parent      
        else:
            level_dict[winner] = kand_level
        
        # We delete the corresponding nodes from frontier.  We remove all edges incident on the nodes in parent_front too (in order to remain a tree).        
        x=[]
        for k in goodEdgesFinal:
            frontier.remove(k)
            for u in parent_front.values():
                if k in u:
                    u.remove(k)
            x.extend(H.in_edges(k, data=True)) # This gets the edge weights
            x.remove((winner, k, H.get_edge_data(winner, k)))
            
        removedEdges.extend(x)
        H.remove_edges_from(x)
                    
        # We remove empty values from the dictionary.
        parent_front = dict((k, v) for k, v in parent_front.items() if v)
        
        # We add the winner to the frontier, and add the parent of the winner to parent_front.
        if winner not in Steiner:
            frontier.add(winner) 
            for j in H.in_edges(winner):
                if j[0] in parent_front:
                    parent_front[j[0]].add(j[1])
                else: parent_front[j[0]] = {j[1]}

    H.add_edges_from(removedEdges)
    
    return Steiner

def SteinerDir3(H, root, query, maxlevel, V):
    # This Algo selects the 'best' edge, based on the highest gain in IR amongst all the possible edges that can be added. 
    # weight_dict is an edge probability dict.
    # H should be the output of createShortestPaths

    # Steiner is a forest dict.
    Steiner = {root: None}
    q_len = len(query)

    # Some necessary checks to ensure the tree we return doesn't exceed the maxlevel property.  
    SP_dict = nx.shortest_path_length(H, source=root) # A shortest path lengths dict. from the root node to all the other nodes. 
    
    frontier = set(query)
    try: frontier.remove(root)
    except KeyError: pass

    level_dict = dict((k, 0) for k in frontier) # A level dict for the Steiner tree.    

        
    parent_front = {}
    # This is a dict with parents as keys and with values are sets of nodes in frontier.
    # Initialise parent_front.
    for i in frontier:
        for j in H.in_edges(i):
            if j[0] in parent_front:
                parent_front[j[0]].add(j[1])
            else: parent_front[j[0]] = {j[1]}

    removedEdges = []
    while frontier:
        # This finds the best node (with the highest total weight to the frontier nodes.)
        max_gewicht = -100000
        #print(frontier)
        gewichtenLijst = []
        edgeLijst = []
        for kandidaat_node in parent_front:
            if (kandidaat_node in Steiner or kandidaat_node in Steiner.values() or kandidaat_node in query):
                n = len(set(Steiner.keys()).union(query))
            else:
                n = len(set(Steiner.keys()).union(query))+1
            for x in parent_front[kandidaat_node]:
                kandidaat_gewicht = H[kandidaat_node][x]['weight']/((n-q_len+1)*log(V-q_len+1,2)+n*log(n+1,2))
                gewichtenLijst.append(kandidaat_gewicht)
                edgeLijst.append((x, kandidaat_node))
        
        # Finding the highest IR edge that CAN be added.
        indices = [i[0] for i in sorted(enumerate(gewichtenLijst), key=lambda x:x[1], reverse=True)] # This tells you what indices to look for in edgeLijst.
        teller = 0
        while True:
            source = edgeLijst[indices[teller]][0]
            target = edgeLijst[indices[teller]][1]
            check = checkChildren2(H, source, target, SP_dict, maxlevel, Steiner, level_dict, query)
            if len(check)==2:
                winner2 = source
                winner = target
                SP_kand = check[1]
                break
            teller += 1
        
        # Updating SP_dict:
        SP_dict.update(SP_kand)
                        
        # Add the result to Steiner (which is a forrest, but finalizes as a tree).
        Steiner.update({winner2: winner})

        # Updating the level dict.
        kand_level = level_dict[winner2]+1 
        if winner in level_dict:
            if kand_level > level_dict[winner]:
                level_dict[winner] = kand_level
                start = winner
                while start in Steiner and start is not root:
                    parent = Steiner[start]
                    if level_dict[parent] >= level_dict[start]+1:
                        start = None
                    else:
                        level_dict[parent] = level_dict[start]+1
                        start = parent      
        else:
            level_dict[winner] = kand_level
  
        # We delete the corresponding nodes from frontier.  We remove all edges incident on the nodes in parent_front too (in order to remain a tree).
        frontier.remove(winner2)
        for u in parent_front.values():
            if winner2 in u:
                u.remove(winner2)

        x = H.in_edges(winner2, data=True)
        x.remove((winner, winner2, H.get_edge_data(winner, winner2)))
                 
        H.remove_edges_from(x)
        removedEdges.extend(x)
                
        # We remove empty values from the dictionairy.
        parent_front = dict((k, v) for k, v in parent_front.items() if v)
        
        # We add the winner to the frontier, and add the parent of the winner to parent_front.
        if winner not in Steiner:
            frontier.add(winner) 
            for j in H.in_edges(winner):
                if j[0] in parent_front:
                    parent_front[j[0]].add(j[1])
                else: parent_front[j[0]] = {j[1]}

    H.add_edges_from(removedEdges)
    return Steiner


def SteinerDir4(H, root, query, maxlevel, V):
    # This adds the 'best' edge from the 'best' parent node. 'best' parent node is based on the possible IR gain of adding all it's edges to frontier nodes. 

    # weight_dict is an edge probability dict.
    # Greedy algorithm for finding a (downward) Steiner Arborescence with maximally informative edges. Assume the level of the root is <= k.
    # Apply Grev for a upward Steiner Arborescence.
    # We start by creating a subgraph with ALL the paths from r->q_i. The result is a DAG. If k is small, this is feasible.
    Steiner = {root: None}
    q_len = len(query)

    # Some necessary checks to ensure the tree we return doesn't exceed the maxlevel property.  
    SP_dict = nx.shortest_path_length(H, source=root) # A shortest path lengths dict. from the root node to all the other nodes. 
     
    frontier = set(query)
    try: frontier.remove(root)
    except KeyError: pass

    level_dict = dict((k, 0) for k in frontier) # A level dict for the Steiner tree.
        
    parent_front = {}
    # This is a dict with parents as keys and with values are sets of nodes in frontier.
    # Initialise parent_front.
    for i in frontier:
        for j in H.in_edges(i):
            if j[0] in parent_front:
                parent_front[j[0]].add(j[1])
            else: parent_front[j[0]] = {j[1]}

    removedEdges = []
    while frontier:
        # This finds the best 'parent' node (with the highest total edge weights to the frontier nodes.)
        max_gewicht = -100000
        for kandidaat_node in parent_front:
            if (kandidaat_node in Steiner or kandidaat_node in Steiner.values() or kandidaat_node in query): #and (kandidaat_node is not root):
                n = len(set(Steiner.keys()).union(query))  
            else:
                n = len(set(Steiner.keys()).union(query))+1
            # Only taking in account edges that don't violate the maxlevel property!
            goodEdges = [x for x in parent_front[kandidaat_node] if checkChildren2(H, x, kandidaat_node, SP_dict, maxlevel, Steiner, level_dict, query)[0]]
            kandidaat_gewicht = sum(H[kandidaat_node][x]['weight'] for x in goodEdges)/((n-q_len+1)*log(V-q_len+1,2)+n*log(n+1,2)) 
            if kandidaat_gewicht > max_gewicht:
                goodEdgesFinal = goodEdges
                max_gewicht = kandidaat_gewicht
                winner = kandidaat_node

        max_gewicht = -100000        
        for x in goodEdgesFinal:
            kandidaat_gewicht = H[winner][x]['weight']
            if kandidaat_gewicht > max_gewicht:
                max_gewicht = kandidaat_gewicht
                winner2 = x

        # Update SP_dict
        SP_dict.update(checkChildren2(H, winner2, winner, SP_dict, maxlevel, Steiner, level_dict, query)[1])        
              
        # Add the result to Steiner (which is a forrest, but finalizes as a tree).
        Steiner.update({winner2: winner})

        # Updating the level of the winner (taking in account that the winner may already be in the Steiner tree)
        kand_level = level_dict[winner2] + 1
        
        if winner in level_dict:
            if kand_level > level_dict[winner]:
                level_dict[winner] = kand_level
                start = winner
                while start in Steiner and start is not root:
                    parent = Steiner[start]
                    if level_dict[parent] >= level_dict[start]+1:
                        start = None
                    else:
                        level_dict[parent] = level_dict[start]+1
                        start = parent      
        else:
            level_dict[winner] = kand_level
       
        # We delete the corresponding nodes from frontier.  We remove all edges incident on the nodes in parent_front too (in order to remain a tree).
        frontier.remove(winner2)
        for u in parent_front.values():
            if winner2 in u:
                u.remove(winner2)

        x = H.in_edges(winner2, data=True)
        x.remove((winner, winner2, H.get_edge_data(winner, winner2)))
        H.remove_edges_from(x)
        removedEdges.extend(x)
                
        # We remove empty values from the dictionairy.
        parent_front = dict((k, v) for k, v in parent_front.items() if v)
        
        # We add the winner to the frontier, and add the parent of the winner to parent_front.
        if winner not in Steiner:
            frontier.add(winner) 
            for j in H.in_edges(winner):
                if j[0] in parent_front:
                    parent_front[j[0]].add(j[1])
                else: parent_front[j[0]] = {j[1]}

    H.add_edges_from(removedEdges)
    return Steiner


################## FUNCTIONS TO GENERATE DATA (ECML-PKDD experiments) ##########################

def TimingAlgo(G, Grev, numberoftrees, querysize, beamwidth, s, tree_depth, weight_dict):
    with open('TimingNEWALGO.txt', 'a') as Timing:
        teller = 0
        V = len(G.nodes())
        while teller < numberoftrees:
            init_node = random.choice(G.nodes())
            query = GenerateQuerySet(G, init_node, querysize, beamwidth, s)
            commonRoots, pruned_nodes_Query = Common(G, query, tree_depth)     
            if len(query) == querysize and len(commonRoots)>0:
                random_root = random.choice(list(commonRoots))
                print(random_root, flush=True)
                print(query, flush=True)
                
                t0 = time()
                H, allPaths, nodeToPath = PreProcessing(G, Grev, random_root, query, pruned_nodes_Query, tree_depth, weight_dict)
                t1 = time()
                time_SP = t1-t0
                
                t0 = time()
                S1 = SteinerBestEdge(H, random_root, query, allPaths, nodeToPath, V)
                t1 = time()
                time_s1 = t1-t0
                
                
                t0 = time()
                S1 = SteinerBestIC(H, random_root, query, allPaths, nodeToPath, V)
                t1 = time()
                time_s2 = t1-t0
                
                
                t0 = time()
                S1 = SteinerBestIR(H, random_root, query, allPaths, nodeToPath, V)
                t1 = time()
                time_s3 = t1-t0
                
                
                t0 = time()
                S1 = SteinerBestEdgeBestIR(H, random_root, query, allPaths, nodeToPath, V)
                t1 = time()
                time_s4 = t1-t0
                
     
                Timing.write(repr(time_SP)+'\t'+repr(time_s1)+'\t'+repr(time_s2)+'\t'+repr(time_s3)+'\t'+repr(time_s4)+'\n')
                teller +=1
                print(teller, flush=True)


                
def HeuristicsAllTrees(G, Grev, query, maxlevel, d_weights):
    # We return the best trees over all roots of a certain maxlevel.
    
    V = len(G)
    q = len(query)
    common_r = Common(G, query, maxlevel)[0]
    max_ir_s1 = 0
    max_ir_s2 = 0
    max_ir_s3 = 0
    max_ir_s4 = 0
    for root in common_r:
        s1 = SteinerDir1(Grev, root, query, maxlevel, d_weights)
        kand_s1 = ICup(s1, d_weights)/DLTree(s1, q, V)
        if kand_s1 > max_ir_s1:
            maxs1 = s1
            max_ir_s1 = kand_s1

        s2 = SteinerDir2(Grev, root, query, maxlevel, d_weights, V)
        kand_s2 = ICup(s2, d_weights)/DLTree(s2, q, V)
        if kand_s2 > max_ir_s2:
            maxs2 = s2
            max_ir_s2 = kand_s2

        s3 = SteinerDir3(Grev, root, query, maxlevel, d_weights, V)
        kand_s3 = ICup(s3, d_weights)/DLTree(s3, q, V)
        if kand_s3 > max_ir_s3:
            maxs3 = s3
            max_ir_s3 = kand_s3

        s4 = SteinerDir4(Grev, root, query, maxlevel, d_weights, V)
        kand_s4 = ICup(s4, d_weights)/DLTree(s4, q, V)
        if kand_s4 > max_ir_s4:
            maxs4 = s4
            max_ir_s4 = kand_s4

    return maxs1, maxs2, maxs3, maxs4
        
            
        

def AverageJaccardTree(Tree, d_meta):
    # Compute the average Jaccard DISTANCE in a tree, with authors as labels.
    # Find the root
    root = list(Tree.keys())[list(Tree.values()).index(None)]

    somJaccard = 0
    for x in Tree:
        if x != root:
            authors_x = set(d_meta[x][1].split(', '))
            authors_Tx = set(d_meta[Tree[x]][1].split(', '))
            common = len(authors_x.intersection(authors_Tx))
            uni = len(authors_x.union(authors_Tx))
            somJaccard += 1-common/uni
    return somJaccard/(len(Tree)-1)

def CommonAuthorsTree(Tree, d_meta):
    # Compute the average Jaccard DISTANCE in a tree, with authors as labels.
    # Find the root
    root = list(Tree.keys())[list(Tree.values()).index(None)]

    somCommon = 0
    for x in Tree:
        if x != root:
            authors_x = set(d_meta[x][1].split(', '))
            authors_Tx = set(d_meta[Tree[x]][1].split(', '))
            common = len(authors_x.intersection(authors_Tx))
            somCommon += common
    return somCommon
    

def GenerateQuerySet(G, node_initial, k, beamwidth, s):
    initNeighbors = G.neighbors(node_initial)
    cands = set(random.sample(initNeighbors, min(len(initNeighbors),beamwidth)))
    neighborsPicked = set()
    neighborsPicked.add(node_initial)
    query = set()
    query.add(node_initial)
    while len(query) < k and len(cands)>0:
        queryaddtest = set()
        candsNow = cands.copy()
        for i in candsNow:
            if random.uniform(0,1) < s:
                queryaddtest.add(i)
                cands.remove(i)
            if i not in neighborsPicked:
                neighborsPicked.add(i)
                iNeighbors = G.neighbors(i)
                cands.update(set(random.sample(iNeighbors, min(len(iNeighbors),beamwidth))))
        query.update(set(random.sample(queryaddtest,min(k-len(query),len(queryaddtest)))))
    return query

def GenerateQuerySetSameAuthors(G, node_initial, k, beamwidth, s, d_meta):
    # The same as GenerateQuerySet, but with preference for overlapping authors.
    initNeighbors = G.neighbors(node_initial)
    cands = set(random.sample(initNeighbors, min(len(initNeighbors),beamwidth)))
    neighborsPicked = set()
    neighborsPicked.add(node_initial)
    query = set()
    query.add(node_initial)
    querySetAuthors = set(d_meta[node_initial][1].split(', '))
    while len(query) < k and len(cands)>0:
        queryaddtest1 = set()
        queryaddtest2 = set()
        candsNow = cands.copy()
        for i in candsNow:
            author_i = set(d_meta[i][1].split(', '))
            if len(author_i.intersection(querySetAuthors)) > 0:
                queryaddtest1.add(i)
                cands.remove(i)
            elif random.uniform(0,1) < s:
                queryaddtest2.add(i)
                cands.remove(i)
            if i not in neighborsPicked:
                neighborsPicked.add(i)
                iNeighbors = G.neighbors(i)
                cands.update(set(random.sample(iNeighbors, min(len(iNeighbors),beamwidth))))
        kand1 = set(random.sample(queryaddtest1,min(k-len(query),len(queryaddtest1))))
        kand2 = kand1.union(set(random.sample(queryaddtest2,min(k-len(query)-len(kand1),len(queryaddtest2)))))
        query.update(kand2)
        for ka in kand2:
            querySetAuthors.update(set(d_meta[ka][1].split(', ')))
    return query

def GenerateData(G, Grev, numberoftrees, querysize, beamwidth, s, tree_depth, d_weights):
    with open('NAlgoData_Q5_n200_s01.txt', 'a') as algoIR, open('TimingQ5.txt', 'a') as Timing:
        MAX_TREES = 200000
        teller = 0
        V = len(G.nodes())
        while teller < numberoftrees:
            init_node = random.choice(G.nodes())
            query = GenerateQuerySet(G, init_node, querysize, beamwidth, s)
            commonRoots, pruned_space = Common(G, query, tree_depth)[0]
            if len(query) == querysize and len(commonRoots)>0:
                random_root = random.choice(list(commonRoots))
                Trees = AllTreesOneRoot(Grev, query, random_root, tree_depth, MAX_TREES) # Bounded! 
                if len(Trees) > 10 and len(Trees) < MAX_TREES:
                    t0 = time()
                    H = createShortestPaths(Grev, pruned_space, random_root, query, tree_depth, d_weights)
                    t1 = time()
                    timeSP = t1-t0
                    try:
                        t0 = time()
                        S1 = SteinerDir1(H, random_root, query, tree_depth)
                        t1 = time()
                        time_s1 = t1-t0
                        IR_S1 = round(ICup(S1, d_weights)/DLTree(S1, querysize, V),5)
                    except:
                        print('S1', random_root, query)
                        continue
                    try:
                        t0 = time()
                        S2 = SteinerDir2(H, random_root, query, tree_depth, V)
                        t1 = time()
                        time_s2 = t1-t0
                        IR_S2 = round(ICup(S2, d_weights)/DLTree(S2, querysize, V),5)
                    except:
                        print('S2', random_root, query)
                        continue
                    try:
                        t0 = time()
                        S3 = SteinerDir3(H, random_root, query, tree_depth, V)
                        t1 = time()
                        time_s3 = t1-t0
                        IR_S3 = round(ICup(S3, d_weights)/DLTree(S3, querysize, V),5)
                    except:
                        print('S3', random_root, query)
                        continue
                    try:
                        t0 = time()
                        S4 = SteinerDir4(H, random_root, query, tree_depth, V)
                        t1 = time()
                        time_s4 = t1-t0
                        IR_S4 = round(ICup(S4, d_weights)/DLTree(S4, querysize, V),5)
                    except:
                        print('S4', random_root, query)
                        continue
                    MAXIR = maxIRup(Trees, d_weights, querysize, V)[1]
                    ratio_avg = avgIRup(Trees, d_weights, querysize, V)/MAXIR
                    ratio_s1 = IR_S1/MAXIR
                    ratio_s2 = IR_S2/MAXIR
                    ratio_s3 = IR_S3/MAXIR
                    ratio_s4 = IR_S4/MAXIR
                    if ratio_s1 > 1: print('S1 ratio:', ratio_s1, random_root, query)
                    if ratio_s2 > 1: print('S2 ratio', ratio_s2, random_root, query)
                    if ratio_s3 > 1: print('S3 ratio', ratio_s3, random_root, query)
                    if ratio_s4 > 1: print('S4 ratio', ratio_s4, random_root, query)
                    algoIR.write(repr(ratio_avg)+'\t'+repr(ratio_s1)+'\t'+repr(ratio_s2)+'\t'+repr(ratio_s3)+'\t'+repr(ratio_s4)+'\n')
                    Timing.write(repr(timeSP+time_s1)+'\t'+repr(timeSP+time_s2)+'\t'+repr(timeSP+time_s3)+'\t'+repr(timeSP+time_s4)+'\n')
                    teller += 1
                    print(teller)

def CheckAlgo(G, Grev, numberoftrees, querysize, beamwidth, s, tree_depth, d_weights):
    with open('blabla.txt', 'a') as Timing:
        teller = 0
        V = len(G.nodes())
        while teller < numberoftrees:
            init_node = random.choice(G.nodes())
            query = GenerateQuerySet(G, init_node, querysize, beamwidth, s)
            commonRoots, pruned_space = Common(G, query, tree_depth)     
            if len(query) == querysize and len(commonRoots)>0:
                random_root = random.choice(list(commonRoots))
                print(random_root, flush=True)
                print(query,flush=True)

                t0 = time()
                H = createShortestPaths(Grev, pruned_space, random_root, query, tree_depth, d_weights)
                t1 = time()
                time_SP = t1-t0
                
                t0 = time()
                S1 = SteinerDir1(H, random_root, query, tree_depth)
                t1 = time()
                time_s1 = t1-t0
                
                t0 = time()
                S2 = SteinerDir2(H, random_root, query, tree_depth, V)
                t1 = time()
                time_s2 = t1-t0
                
                t0 = time()
                S3 = SteinerDir3(H, random_root, query, tree_depth, V)
                t1 = time()
                time_s3 = t1-t0
                
                t0 = time()
                S4 = SteinerDir4(H, random_root, query, tree_depth, V)
                t1 = time()
                time_s4 = t1-t0
                   
                Timing.write(repr(time_SP)+'\t'+repr(time_s1)+'\t'+repr(time_s2)+'\t'+repr(time_s3)+'\t'+repr(time_s4)+'\n')
                teller +=1
                print(teller,flush=True)
                

def S2AllRoots(G, Grev, query, maxlevel, weight_dict):
    V = len(G)
    commonRoots, pruned_space = Common(G, query, maxlevel)
    querysize = len(query)
    max_IR = 0
    for root in commonRoots:
        H = createShortestPaths(Grev, pruned_space, root, query, maxlevel, weight_dict)
        S2 = SteinerDir2(H, root, query, maxlevel, V)
        kand_IR = round(ICup(S2, weight_dict)/DLTree(S2, querysize, V),5)
        if kand_IR > max_IR:
            max_IR = kand_IR
            S2_best = S2
    return S2_best, max_IR
        



def checkJaccard(G, Grev, querysize, maxlevel, d_weights, d_weights_b3, d_weights_b5, d_weights_neutral, beamwidth, s, numberofits):
    with open('CommonAuthorsQ9.txt', 'a') as Jac:
        V = len(G)
        teller = 0
        while teller < numberofits:
            init_node = random.choice(G.nodes())
            query = GenerateQuerySetSameAuthors(G, init_node, querysize, 4, 0.2, d_meta)
            commonRoots, pruned_space = Common(G, query, maxlevel)
            if len(query) == querysize and len(commonRoots)>0:
                random_root = random.choice(list(commonRoots))
                H = createShortestPaths(Grev, pruned_space, random_root, query, maxlevel, d_weights)
                S2_degree = SteinerDir2(H, random_root, query, maxlevel, V)

                # Set weights to bin5
                H = createShortestPaths(Grev, pruned_space, random_root, query, maxlevel, d_weights_bin5)
                S2_b5 = SteinerDir2(H, random_root, query, maxlevel, V)

                # Set weights to bin3
                H = createShortestPaths(Grev, pruned_space, random_root, query, maxlevel, d_weights_bin3)
                S2_b3 = SteinerDir2(H, random_root, query, maxlevel, V)

                # Set weights to 1
                H = createShortestPaths(Grev, pruned_space, random_root, query, maxlevel, d_weights_neutral)
                S2_uniform = SteinerDir2(H, random_root, query, maxlevel, V)
         
                Jaccard_uniform = CommonAuthorsTree(S2_uniform, d_meta)
                Jaccard_deg = CommonAuthorsTree(S2_degree, d_meta)
                Jaccard_b5 = CommonAuthorsTree(S2_b5, d_meta)
                Jaccard_b3 = CommonAuthorsTree(S2_b3, d_meta)
                if (Jaccard_uniform == Jaccard_deg and Jaccard_deg == Jaccard_b5 and Jaccard_b3 == Jaccard_b5):
                    print('same')
                else:
                    Jac.write(repr(Jaccard_uniform)+'\t'+repr(Jaccard_deg)+'\t'+repr(Jaccard_b3)+'\t'+repr(Jaccard_b5)+'\n')
                    teller += 1
                    print(teller)


############################################        
                            

        


        
                
                
                

                
                
            
            
        

        
