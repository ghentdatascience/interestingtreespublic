function [t1,t2,t3] = PrepareTest_IndDegree(A)
% Returning 3 arrays as an input for HeuristicPartioningv2
% Assuming a prior on in- and out degree of adj. matrix A
% Sets = [rowsum_1, ... , rowsum_n, columnsum_1, ... , columnsum_n]
t1 = [sum(A,2)' sum(A,1)];
n=length(A);
t2 = n*ones(1,2*n);
% C = {(c_c1, ..., c_cn),(c_r1, ..., c_rn)}
a = sort(t1(1:n));
b = sort(t1(n+1:end));
[~,~,idx]=uniquecell({a,b});
t3=idx([2*ones(1,n),ones(1,n)]);