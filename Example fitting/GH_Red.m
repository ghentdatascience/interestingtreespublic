function [G, H] = GH_Red(x, occur_r, occur_c, d_r, d_c)
% G and H of the Reduced Lagrange dual function, as a function of the reduced lambda variables.
% NO diag. constraints.

% GRADIENT.
l_r = length(occur_r);
l_c = length(occur_c);


C = occur_r'*occur_c;
Ar = repmat(x(1:l_r)', 1, l_c);
Ac = repmat(x(l_r+1:end), l_r, 1);
A = exp(Ar+Ac)./(1+exp(Ar+Ac));
B = A.*C;

G1 = sum(B,2)-occur_r'.*d_r;
G2 = sum(B)'-occur_c'.*d_c;
G = [G1; G2];


% HESSIAN.
Z = B./(1+exp(Ar+Ac));

R = diag(sum(Z, 2));
C = diag(sum(Z));

H = [R Z; Z' C];