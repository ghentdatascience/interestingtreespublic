function [lar, lac, jr, jc] = newton_R(D, nit)
% Newton's method applied to the Lagrange obj function., NO diag. constraints.
% d_r1=d_r2 => la_r1=la_r2 and idem for colums.  
drows = full(sum(D,2));
dcols = full(sum(D,1))';


[nu_r, ~,j_r] = unique(drows); % SORTED!
[nu_c, ~,j_c] = unique(dcols);

% Correcting for the diag. probabilities.
% density = nnz(D)/length(D)/length(D);
% nu_r = nu_r + [0; ones(length(nu_r)-1,1)*length(drows)*density/nnz(drows)];
% nu_c = nu_c + [0; ones(length(nu_c)-1,1)*length(dcols)*density/nnz(dcols)];

l_r = length(nu_r);
occur_r = zeros(1,l_r); % number of occurences of the elements of nu_r.
for i=1:l_r
    occur_r(i)=length(find(j_r==i));
end

l_c = length(nu_c);
occur_c = zeros(1,l_c); % number of occurences of the elements of nu_c.
for i=1:l_c
    occur_c(i)=length(find(j_c==i));
end

% Initial vector x.
x = zeros(1, l_r+l_c);

[Grad, H] = GH_Red(x, occur_r, occur_c, nu_r, nu_c);
epsiH=10^(-10);

alpha=logspace(0,-5, 25);
for k=1:nit
    % Adding a diagonal epsi term to H to make H pos. def.
    delta = -(H+epsiH*eye(l_r+l_c))\Grad;
    
    % Armijo Rule.
    for i=1:length(alpha)
        if obj_R_NODIAG(x+alpha(i)*delta', occur_r, occur_c, nu_r, nu_c) <= obj_R_NODIAG(x, occur_r, occur_c, nu_r, nu_c) + 10^(-1)*alpha(i)*dot(delta,Grad)
            x = x+alpha(i)*delta';
            break
        end
    end
    [Grad, H] = GH_Red(x, occur_r, occur_c, nu_r, nu_c);
end
disp(norm(Grad))
lar = x(1:l_r);
lac = x(l_r+1:end);
jr = j_r;
jc  = j_c;