RunTests_Degree.m is the main file.

PrepareTest_IndDegree.m and HeuristicPartioningV2.m are two functions needed for identifying equal Lagrange multipliers

Newton_R.m is a file for fitting the reduced model.
It needs two other functions, obj_R_NODIAG.m and GH_RED.m - needed to evaluate the (Reduced) Lagrange Dual function and the resp. Gradient and Hessian of that function.


