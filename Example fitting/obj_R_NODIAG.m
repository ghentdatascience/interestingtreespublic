function y = obj_R_NODIAG(x, occur_r, occur_c, d_r, d_c)
% "Reduced"  Lagrange Dual, WITHOUT diag. constraint. 
% Function of the reduced lambdas.
% Reduced, d_r, d_c are vectors with length m <= n.
% x is a vector with length 2m (rows and columns).

l_r = length(occur_r);
l_c = length(occur_c);
som = 0;

for i=1:l_r
    for j=1:l_c
    som  = som + occur_r(i)*occur_c(j)*log(1+exp(x(i)+x(l_r+j)));
    end
    som = som - x(i)*occur_r(i)*d_r(i);
end

for j=1:l_c
    som = som - x(l_r+j)*occur_c(j)*d_c(j);
end 

y = som;