function P = HeuristicPartioningV2(t1, t2, t3)
% Structure similarity test for identyfing equal Lagrange multipliers
% 3 structural tests: 
% 1. c-value  2. Degree 3. Sorted list of the degrees of the neighbors.
% A is an adjacency matrix
% t1 is a 1xm array with the c-values for each set
% t2 is a 1xm array with the degree of each set (the size)
% t3 is a 1xm array, with elements referring to the index of a cell with
% unique tuples.
% Examples t3:
%   Cell of unique tuples C = {{c_1}, {c_1,c_4}, {c_1,c_2,c_3}}
%   t3 = [1 1 3 2 ... 3] referring to the index in C.

% Setting all sets to be equal
Partion = {1:length(t1)};

% The 1st partion test.
NewP = {};
for p = Partion
    t1_partion = t1(p{:});
    [unique_c,~,ic] = unique(t1_partion);
    for i=1:length(unique_c)
        NewP = [NewP, p{:}(ic==i)];
    end
end

% The 2nd partion test.
Partion = NewP;
NewP = {};
for p = Partion
    t2_partion = t2(p{:});
    [unique_d,~,id] = unique(t2_partion);
    for i=1:length(unique_d)
        NewP = [NewP, p{:}(id==i)];
    end
end


% The 3rd partion test
Partion = NewP;
NewP = {};
for p = Partion
    t3_partion = t3(p{:});
    [unique_n,~,in] = unique(t3_partion);
    for i=1:length(unique_n)
        NewP = [NewP, p{:}(in==i)];
    end
end

P = NewP;