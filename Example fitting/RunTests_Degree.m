clear;
clc;

fileID = fopen('DBLP_AA_GCC_edges.txt', 'r'); 
Aconn = fscanf(fileID, '%i', [2 Inf]);
fclose(fileID);
%Aconn=Aconn+ones(2,1049866); % web-Google.txt starts indexing at 0! (add a one to everybody)
numberofnodes=max(unique([Aconn(2,:) Aconn(1,:)]));
A = sparse(Aconn(1,:), Aconn(2,:), ones(1, length(Aconn)), numberofnodes, numberofnodes);
A = (A | A')*1.0; % Amazon is undirected.

% Heuristic Lagr. identifier algo
tic
[t1,t2,t3] = PrepareTest_IndDegree(A);
toc
length(HeuristicPartioningV2(t1,t2,t3))
toc


% Fitting the whole model
tic
[lar, lac, jr, jc] = newton_R(A, 50);
toc

% Writing the weights to a file.
from = Aconn(1,:);
to = Aconn(2,:);

la_from = lar(jr(from));
la_to = lac(jc(to));

weights = exp(la_from+la_to);
weights = weights./(1+weights);

fileID = fopen('edgeProbability.txt', 'w');
fprintf(fileID,'%i %i %2.5e\r\n', [from; to; weights]); % Amazon
%fprintf(fileID,'%i %i %2.5e\r\n', [from-1; to-1; weights]); % DBLP, web-Google
fclose(fileID);